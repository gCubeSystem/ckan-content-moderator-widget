
# Changelog

All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [v1.2.0] - 2023-01-19

#### Enhancements

- [#23903] Catalogue Moderation: allow to send a message to the moderators on reject to pending operation
- [#24519] Added social_post=true to approve request

## [v1.1.1] - 2022-10-27

#### Fixing

- GWT-Servlet at provided

## [v1.1.0] - 2022-08-01

#### Enhancements

- [#23692] Optimized the listing and the paging of catalogue items

## [v1.0.1] - 2022-06-27

- [#23525] Removed the scope of xml-apis dependency
- Moved to maven-portal-bom v3.6.4

## [v1.0.0] - 2022-05-18

- [#21363] Implemented the ckan-content-moderator-widget
- [#20650] Provided moderator skills to Catalogue Moderator(s)
- [#23108] Provided Moderation facility accessible to Catalogue Editor/Admin (only) in read only mode
- [#23197] Revised the query passed to gCat with the moderation states
- [#23258] Implemented the requirement described in #23156
