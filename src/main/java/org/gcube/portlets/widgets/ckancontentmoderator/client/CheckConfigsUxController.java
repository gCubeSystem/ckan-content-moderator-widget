package org.gcube.portlets.widgets.ckancontentmoderator.client;

import org.gcube.datacatalogue.utillibrary.shared.ItemStatus;
import org.gcube.portlets.widgets.ckancontentmoderator.client.ui.MainPanel;
import org.gcube.portlets.widgets.ckancontentmoderator.client.ui.util.LoadingPanel;
import org.gcube.portlets.widgets.ckancontentmoderator.client.ui.util.UtilUx;
import org.gcube.portlets.widgets.ckancontentmoderator.shared.DISPLAY_FIELD;

import com.github.gwtbootstrap.client.ui.constants.AlertType;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.Command;
import com.google.gwt.user.client.ui.ComplexPanel;
import com.google.gwt.user.client.ui.HTML;

/**
 * The Class CheckConfigsUxController.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Apr 8, 2022
 */
public class CheckConfigsUxController {

	private LoadingPanel loader;

	private CkanContentModeratorCheckConfigs moderatorCheckConfig;

	private ComplexPanel basePanelContainer;

	private CkanContentModeratorWidgetController cmsController;

	private MainPanel mainPanel;

	private DISPLAY_FIELD[] orderByFields;

	private ItemStatus initItemStatus;

	private DISPLAY_FIELD[] displayFields;

	private String initOnItemName;

	/**
	 * Instantiates a new check configs ux controller.
	 *
	 * @param mainPanel            the main panel
	 * @param initItemStatus       the init item status
	 * @param initOnItemName
	 * @param displayFields        the display fields
	 * @param sortByFields         the sort by fields
	 * @param moderatorcheckConfig the moderatorcheck config
	 */
	protected CheckConfigsUxController(MainPanel mainPanel, ItemStatus initItemStatus, String initOnItemName,
			DISPLAY_FIELD[] displayFields, DISPLAY_FIELD[] sortByFields,
			CkanContentModeratorCheckConfigs moderatorcheckConfig) {

		this.mainPanel = mainPanel;
		this.basePanelContainer = mainPanel.getMainPanelContainer();
		this.moderatorCheckConfig = moderatorcheckConfig;
		this.initItemStatus = initItemStatus;
		this.initOnItemName = initOnItemName;
		this.displayFields = displayFields;
		this.orderByFields = sortByFields;

		if (this.initItemStatus == null)
			this.initItemStatus = ItemStatus.PENDING;

		if (this.displayFields == null)
			this.displayFields = DISPLAY_FIELD.values();

		if (this.orderByFields == null)
			this.orderByFields = DISPLAY_FIELD.values();

		checkConfigs();
	}

	/**
	 * Check configs.
	 */
	private void checkConfigs() {

		if (moderatorCheckConfig == null) {
			// here the moderatorCheckConfig is null, so loading configurations from server
			// and
			// checking them
			loader = new LoadingPanel(new HTML("Checking configurations and authorizations... please wait"));
			basePanelContainer.add(loader);
			moderatorCheckConfig = new CkanContentModeratorCheckConfigs();

			final Command whenDone = new Command() {

				@Override
				public void execute() {
					onConfigurationsLoaded();
				}
			};

			try {
				moderatorCheckConfig.checkConfigs(whenDone, false);
			} catch (Exception e) {
				GWT.log("Check configs error: " + e.getMessage());
			}
		} else {
			// here the moderatorCheckConfig is already full
			onConfigurationsLoaded();
		}
	}

	/**
	 * On configurations loaded.
	 */
	private void onConfigurationsLoaded() {
		GWT.log("onConfigurationLoaded executed");
		boolean isContentModerationEnabled = false;
		boolean isModeratorRoleAssingned = false;
		boolean isExistsMyItemInModeration = false;

		try {
			isContentModerationEnabled = moderatorCheckConfig.isContentModerationEnabled();
		} catch (Exception e) {
			GWT.log("Command - Check configs error: " + e.getMessage());
		}

		try {
			isModeratorRoleAssingned = moderatorCheckConfig.isModeratorRoleAssigned();
		} catch (Exception e) {
			GWT.log("Command - Check configs error: " + e.getMessage());
		}
		try {
			isExistsMyItemInModeration = moderatorCheckConfig.isExistsMyItemInModeration();
		} catch (Exception e) {
			GWT.log("Command - Check configs error: " + e.getMessage());
		}

		GWT.log("Moderation is enabled? " + isContentModerationEnabled);
		GWT.log("Moderator role is assigned? " + isModeratorRoleAssingned);
		GWT.log("isExistsMyItemInModeration? " + isExistsMyItemInModeration);

		if (!isContentModerationEnabled) {
			try {
				basePanelContainer.remove(loader);
			} catch (Exception e) {
			}
			UtilUx.showAlert("The Moderation facility is not enabled in this context", AlertType.WARNING, false,
					basePanelContainer, null);
			return;
		}

		// moderator logged in
		if (isModeratorRoleAssingned) {
			try {
				basePanelContainer.remove(loader);
			} catch (Exception e) {
			}

			cmsController = new CkanContentModeratorWidgetController(initItemStatus, initOnItemName, displayFields,
					orderByFields, false, false);
			basePanelContainer.add(cmsController.getMainPanel());

			mainPanel.setLoggedLabelText("Logged in as Moderator");
			return;
		}

		// no item under moderation or already moderated
		if (!isExistsMyItemInModeration) {
			try {
				basePanelContainer.remove(loader);
			} catch (Exception e) {
			}
			UtilUx.showAlert("You are not authorized to access to Moderation facility", AlertType.WARNING, false,
					basePanelContainer, null);
			return;
		}

		try {
			basePanelContainer.remove(loader);
		} catch (Exception e) {
		}

		// read only mode enabled in case of moderator role not assigned
		boolean readOnlyMode = !isModeratorRoleAssingned;
		// Listing only data belonging to user logged in. He/She must be the owner of.
		boolean restrictDataToLoggedInUser = readOnlyMode;

		GWT.log("readOnlyMode is enabled? " + readOnlyMode);
		GWT.log("restrictDataToLoggedInUser is? " + restrictDataToLoggedInUser);

		cmsController = new CkanContentModeratorWidgetController(initItemStatus, initOnItemName, displayFields,
				orderByFields, readOnlyMode, restrictDataToLoggedInUser);

		basePanelContainer.add(cmsController.getMainPanel());
	}

	/**
	 * Gets the moderator check config.
	 *
	 * @return the moderator check config
	 */
	public CkanContentModeratorCheckConfigs getModeratorCheckConfig() {
		return moderatorCheckConfig;
	}

}
