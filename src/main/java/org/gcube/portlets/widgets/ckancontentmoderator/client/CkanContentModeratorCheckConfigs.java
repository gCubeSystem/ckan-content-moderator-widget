package org.gcube.portlets.widgets.ckancontentmoderator.client;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.Command;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.rpc.AsyncCallback;

/**
 * The Class CkanContentModeratorCheckConfigs.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Apr 8, 2022
 */
public class CkanContentModeratorCheckConfigs {

	private int configurationLoaded = 0;
	private static final int CONFIGURATION_EXPECTED = 3;
	private int MAX_RETRY_ON_LOADING_CONFIG = 50;
	private int attemptLC = 0;
	private Boolean contentModerationEnabled = null;
	private Boolean moderatorRoleAssigned = null;
	private Boolean existsMyItemInModeration = null;

	/**
	 * Instantiates a new ckan content moderator check config.
	 */
	public CkanContentModeratorCheckConfigs() {

	}

	/**
	 * Check configs.
	 *
	 * @param whenDone         the when done
	 * @param reloadGCatConfig the reload G cat config
	 * @throws Exception the exception
	 */
	public void checkConfigs(final Command whenDone, boolean reloadGCatConfig) throws Exception {

		configurationLoaded = 0;
		attemptLC = 0;

		CkanContentModeratorWidgetController.contentModeratorService.isModerationEnabled(reloadGCatConfig,
				new AsyncCallback<Boolean>() {

					@Override
					public void onFailure(Throwable caught) {
						contentModerationEnabled = false;
						incrementConfigurationLoaded();

					}

					@Override
					public void onSuccess(Boolean result) {
						incrementConfigurationLoaded();
						GWT.log("isContentModeratorEnabled: " + result);
						contentModerationEnabled = result;
					}
				});

		CkanContentModeratorWidgetController.contentModeratorService
				.isModeratorRoleAssigned(new AsyncCallback<Boolean>() {

					@Override
					public void onFailure(Throwable caught) {
						moderatorRoleAssigned = false;
						incrementConfigurationLoaded();

					}

					@Override
					public void onSuccess(Boolean result) {
						incrementConfigurationLoaded();
						GWT.log("isModeratorRoleAssigned: " + result);
						moderatorRoleAssigned = result;

					}
				});

		CkanContentModeratorWidgetController.contentModeratorService
				.existsMyItemInModeration(new AsyncCallback<Boolean>() {

					@Override
					public void onFailure(Throwable caught) {
						existsMyItemInModeration = false;
						incrementConfigurationLoaded();

					}

					@Override
					public void onSuccess(Boolean result) {
						incrementConfigurationLoaded();
						GWT.log("existsMyItemInModeration: " + result);
						existsMyItemInModeration = result;

					}
				});

		if (whenDone != null) {

			final Timer timer = new Timer() {

				@Override
				public void run() {
					attemptLC++;
					GWT.log("checking configuration loaded, attempt " + attemptLC + " of "
							+ MAX_RETRY_ON_LOADING_CONFIG);
					int confLoaded = getConfigurationLoaded();
					GWT.log("configuration loaded " + confLoaded + " of "
							+ CONFIGURATION_EXPECTED);
					boolean configsLoaded = confLoaded == CONFIGURATION_EXPECTED;
					GWT.log("configsLoaded: " + configsLoaded);
					if (configsLoaded) {
						GWT.log("ContentModeratorCheckConfig loaded correclty");
						whenDone.execute();
						this.cancel();
					}

					if (attemptLC > MAX_RETRY_ON_LOADING_CONFIG) {
						GWT.log("MAX_RETRY_ON_LOADING_CONFIG reached, timer cancelled");
						this.cancel();
					}

				}
			};

			timer.scheduleRepeating(500);
		}
	}

	/**
	 * Increment configuration loaded.
	 */
	private void incrementConfigurationLoaded() {
		configurationLoaded++;
	}

	/**
	 * Decrement configuration loaded.
	 */
	private synchronized void decrementConfigurationLoaded() {
		configurationLoaded--;
	}

	/**
	 * Gets the configuration loaded.
	 *
	 * @return the configuration loaded
	 */
	private synchronized int getConfigurationLoaded() {
		return configurationLoaded;
	}

	/**
	 * Checks if is moderator role assigned.
	 *
	 * @return the boolean
	 * @throws Exception the exception
	 */
	public Boolean isModeratorRoleAssigned() throws Exception {
		if (moderatorRoleAssigned == null)
			throw new Exception(
					"Please, first check if the Moderator role is assigned in this context by calling checkContentModeratorConfiguration");

		return moderatorRoleAssigned;
	}

	/**
	 * Checks if is content moderation enabled.
	 *
	 * @return true, if is content moderation enabled
	 * @throws Exception the exception
	 */
	public boolean isContentModerationEnabled() throws Exception {

		if (contentModerationEnabled == null)
			throw new Exception(
					"Please, first check if the content moderation is enabled in this context by calling checkContentModeratorConfiguration");

		return contentModerationEnabled;

	}

	/**
	 * Checks if is exists my item in moderation.
	 *
	 * @return the boolean
	 * @throws Exception the exception
	 */
	public Boolean isExistsMyItemInModeration() throws Exception {
		if (existsMyItemInModeration == null)
			throw new Exception(
					"Please, first check if the content moderation is enabled in this context by calling checkContentModeratorConfiguration");

		return existsMyItemInModeration;
	}

}
