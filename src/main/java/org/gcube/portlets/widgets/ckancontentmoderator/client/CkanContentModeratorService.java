package org.gcube.portlets.widgets.ckancontentmoderator.client;

import java.util.List;

import org.gcube.datacatalogue.utillibrary.shared.ItemStatus;
import org.gcube.portlets.widgets.ckancontentmoderator.shared.CatalogueDataset;
import org.gcube.portlets.widgets.ckancontentmoderator.shared.ModerationUserRole;
import org.gcube.portlets.widgets.ckancontentmoderator.shared.OperationReport;
import org.gcube.portlets.widgets.ckancontentmoderator.shared.SearchedData;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

/**
 * The Interface CkanContentModeratorService.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Jan 11, 2022
 */
@RemoteServiceRelativePath("ckanContentModeratorService")
public interface CkanContentModeratorService extends RemoteService {

	/**
	 * Reject item.
	 *
	 * @param itemNames         the item names
	 * @param permanentlyDelete the permanently delete
	 * @param reasonMsg         the reason msg
	 * @return the operation report
	 * @throws Exception the exception
	 */
	public OperationReport rejectItem(List<String> itemNames, boolean permanentlyDelete, String reasonMsg)
			throws Exception;

	/**
	 * Permanently delete.
	 *
	 * @param itemNames the item names
	 * @return the operation report
	 * @throws Exception the exception
	 */
	public OperationReport permanentlyDelete(List<String> itemNames) throws Exception;

	/**
	 * Gets the list items for status.
	 *
	 * @param theStatus                the the status
	 * @param offset                   the offset
	 * @param limit                    the limit
	 * @param allFields                the all fields. If true returns the all
	 *                                 fields of an item
	 * @param restrictedToLoggedInUser if true restricts the list of items to logged
	 *                                 in user
	 * @param sortForField             the sort for field
	 * @return the list items for status
	 * @throws Exception the exception
	 */
	public List<CatalogueDataset> getListItemsForStatus(ItemStatus theStatus, int offset, int limit, boolean allFields,
			boolean restrictedToLoggedInUser, String sortForField) throws Exception;

	/**
	 * Gets the data for status.
	 *
	 * @param status                   the status
	 * @param offset                   the offset
	 * @param limit                    the limit
	 * @param serverStartIndex         the server start index
	 * @param restrictedToLoggedInUser the restricted to logged in user
	 * @param sortForField             the sort for field
	 * @return the data for status
	 * @throws Exception the exception
	 */
	public SearchedData getDataForStatus(ItemStatus status, int offset, int limit, int serverStartIndex,
			boolean restrictedToLoggedInUser, String sortForField) throws Exception;

	/**
	 * Approve item.
	 *
	 * @param itemNames        the item names
	 * @param moderatorMessage the moderator message
	 * @return the operation report
	 * @throws Exception the exception
	 */
	public OperationReport approveItem(List<String> itemNames, String moderatorMessage) throws Exception;

	/**
	 * Gets the CMS roles for user in the context.
	 *
	 * @return the CMS roles for user in the context
	 * @throws Exception the exception
	 */
	public ModerationUserRole getCMSRolesForUserInTheContext() throws Exception;

	/**
	 * Checks if is moderation enabled.
	 *
	 * @param reloadConfig the reload config
	 * @return the boolean
	 */
	public Boolean isModerationEnabled(boolean reloadConfig);

	/**
	 * Checks if is moderator role assigned.
	 *
	 * @return true, if is moderat@Override or role assigned
	 * @throws Exception the exception
	 */
	public Boolean isModeratorRoleAssigned() throws Exception;

	/**
	 * Exists my item in moderation.
	 *
	 * @return true if the user has at least one item moderatorated or in
	 *         moderation.
	 * @throws Exception the exception
	 */
	public Boolean existsMyItemInModeration() throws Exception;

	/**
	 * Gets the item for name.
	 *
	 * @param itemName the item name
	 * @return the item for name
	 * @throws Exception the exception
	 */
	public CatalogueDataset getItemForName(String itemName) throws Exception;

	/**
	 * Sets the status.
	 *
	 * @param theStatus the the status
	 * @param itemNames the item names
	 * @return the operation report
	 * @throws Exception the exception
	 */
	public OperationReport setStatus(ItemStatus theStatus, List<String> itemNames, String message) throws Exception;

}
