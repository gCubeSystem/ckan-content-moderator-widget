package org.gcube.portlets.widgets.ckancontentmoderator.client;

import java.util.List;

import org.gcube.datacatalogue.utillibrary.shared.ItemStatus;
import org.gcube.portlets.widgets.ckancontentmoderator.shared.CatalogueDataset;
import org.gcube.portlets.widgets.ckancontentmoderator.shared.ModerationUserRole;
import org.gcube.portlets.widgets.ckancontentmoderator.shared.OperationReport;
import org.gcube.portlets.widgets.ckancontentmoderator.shared.SearchedData;

import com.google.gwt.user.client.rpc.AsyncCallback;

/**
 * The Interface CkanContentModeratorServiceAsync.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Jan 11, 2022
 */
public interface CkanContentModeratorServiceAsync {

	/**
	 * Reject item.
	 *
	 * @param itemNames         the item names
	 * @param permanentlyDelete the permanently delete
	 * @param reasonMsg         the reason msg
	 * @param callback          the callback
	 */
	void rejectItem(List<String> itemNames, boolean permanentlyDelete, String reasonMsg,
			AsyncCallback<OperationReport> callback);

	/**
	 * Gets the list items for status.
	 *
	 * @param theStatus                the the status
	 * @param offset                   the offset
	 * @param limit                    the limit
	 * @param allFields                the all fields
	 * @param restrictedToLoggedInUser the restricted to logged in user
	 * @param sortForField             the sort for field
	 * @param callback                 the callback
	 * @return the list items for status
	 */
	void getListItemsForStatus(ItemStatus theStatus, int offset, int limit, boolean allFields,
			boolean restrictedToLoggedInUser, String sortForField, AsyncCallback<List<CatalogueDataset>> callback);

	/**
	 * Gets the data for status.
	 *
	 * @param status                   the status
	 * @param offset                   the offset
	 * @param limit                    the limit
	 * @param serverStartIndex         the server start index
	 * @param restrictedToLoggedInUser the restricted to logged in user
	 * @param sortForField             the sort for field
	 * @param asyncCallback            the async callback
	 * @return the data for status
	 */
	void getDataForStatus(ItemStatus status, int offset, int limit, int serverStartIndex,
			boolean restrictedToLoggedInUser, String sortForField, AsyncCallback<SearchedData> asyncCallback);

	/**
	 * Approve item.
	 *
	 * @param itemNames        the item names
	 * @param moderatorMessage the moderator message
	 * @param callback         the callback
	 */
	void approveItem(List<String> itemNames, String moderatorMessage, AsyncCallback<OperationReport> callback);

	/**
	 * Permanently delete.
	 *
	 * @param itemNames the item names
	 * @param callback  the callback
	 */
	void permanentlyDelete(List<String> itemNames, AsyncCallback<OperationReport> callback);

	/**
	 * Sets the status.
	 *
	 * @param theStatus the the status
	 * @param itemNames the item names
	 * @param message   the message
	 * @param callback  the callback
	 */
	void setStatus(ItemStatus theStatus, List<String> itemNames, String message,
			AsyncCallback<OperationReport> callback);

	/**
	 * Gets the CMS roles for user in the context.
	 *
	 * @param callback the callback
	 * @return the CMS roles for user in the context
	 */
	void getCMSRolesForUserInTheContext(AsyncCallback<ModerationUserRole> callback);

	/**
	 * Checks if is moderation enabled.
	 *
	 * @param reloadConfig the reload config
	 * @param callback     the callback
	 */
	void isModerationEnabled(boolean reloadConfig, AsyncCallback<Boolean> callback);

	/**
	 * Checks if is moderator role assigned.
	 *
	 * @param callback the callback
	 */
	void isModeratorRoleAssigned(AsyncCallback<Boolean> callback);

	/**
	 * Exists my item in moderation.
	 *
	 * @param callback the callback
	 * @return true if the user has at least one item moderatorated or in
	 *         moderation.
	 */
	void existsMyItemInModeration(AsyncCallback<Boolean> callback);

	/**
	 * Gets the item for name.
	 *
	 * @param itemName the item name
	 * @param callback the callback
	 * @return the item for name
	 */
	void getItemForName(String itemName, AsyncCallback<CatalogueDataset> callback);

}
