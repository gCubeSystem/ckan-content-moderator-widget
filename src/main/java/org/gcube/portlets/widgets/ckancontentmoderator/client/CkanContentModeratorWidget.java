package org.gcube.portlets.widgets.ckancontentmoderator.client;

import org.gcube.datacatalogue.utillibrary.shared.ItemStatus;
import org.gcube.portlets.widgets.ckancontentmoderator.client.ui.MainPanel;
import org.gcube.portlets.widgets.ckancontentmoderator.client.ui.util.ExtModal;
import org.gcube.portlets.widgets.ckancontentmoderator.client.util.ModerationQueryStringUtil.ModerationBuilder;
import org.gcube.portlets.widgets.ckancontentmoderator.shared.DISPLAY_FIELD;

import com.github.gwtbootstrap.client.ui.Modal;
import com.google.gwt.user.client.ui.Composite;

/**
 * The Class CkanContentModeratorWidget.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Apr 7, 2022
 */
public class CkanContentModeratorWidget {

	private MainPanel mainPanel = new MainPanel();

	private CheckConfigsUxController ccux;

	/**
	 * Instantiates a new ckan content moderator widget.
	 *
	 * @param initItemStatus the init item status
	 * @param initOnItemName the init on item name
	 * @param displayFields  the display fields
	 * @param sortByFields   the sort by fields
	 */
	public CkanContentModeratorWidget(ItemStatus initItemStatus, String initOnItemName, DISPLAY_FIELD[] displayFields,
			DISPLAY_FIELD[] sortByFields) {
		mainPanel.setWidth("100%");
		ccux = new CheckConfigsUxController(mainPanel, initItemStatus, initOnItemName, displayFields, sortByFields, null);

		boolean isModeratorRoleAssigned = false;
		try {
			isModeratorRoleAssigned = ccux.getModeratorCheckConfig().isModeratorRoleAssigned();
		} catch (Exception e) {

		}

		if (isModeratorRoleAssigned) {
			mainPanel.setLoggedLabelText("Logged in as Moderator");
		}
	}

	/**
	 * Instantiates a new ckan content moderator widget.
	 *
	 * @param builder the builder
	 */
	public CkanContentModeratorWidget(ModerationBuilder builder) {
		this(builder.getItemStatus(), builder.getItemName(), builder.getDisplayFields(), builder.getSortByFields());
	}

	/**
	 * Show as modal.
	 *
	 * @param modalTitle the modal title
	 */
	public void showAsModal(String modalTitle) {
		final Modal modal = new ExtModal(true, true);
		modalTitle = modalTitle == null || modalTitle.isEmpty() ? "Manage Items" : modalTitle;
		modal.setTitle(modalTitle);
		modal.setWidth(1200);
		modal.getElement().addClassName("modal-content-moderator");
		// modal.getElement().getStyle().setProperty("min-height", "500px");
		modal.add(mainPanel);
		modal.setCloseVisible(true);
		modal.show();
	}

	/**
	 * Gets the panel.
	 *
	 * @return the panel
	 */
	public Composite getPanel() {
		return mainPanel;
	}

	/**
	 * Gets the congifs.
	 *
	 * @return the congifs
	 */
	public CkanContentModeratorCheckConfigs getCongifs() {
		return ccux.getModeratorCheckConfig();
	}
}
