package org.gcube.portlets.widgets.ckancontentmoderator.client;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import org.gcube.datacatalogue.utillibrary.shared.ItemStatus;
import org.gcube.portlets.widgets.ckancontentmoderator.client.events.CheckBoxSelectIemsEvent;
import org.gcube.portlets.widgets.ckancontentmoderator.client.events.CheckBoxSelectIemsEventHandler;
import org.gcube.portlets.widgets.ckancontentmoderator.client.events.ClickItemEvent;
import org.gcube.portlets.widgets.ckancontentmoderator.client.events.ClickItemEventHandler;
import org.gcube.portlets.widgets.ckancontentmoderator.client.events.CloseAllTabsEvent;
import org.gcube.portlets.widgets.ckancontentmoderator.client.events.CloseAllTabsEventHandler;
import org.gcube.portlets.widgets.ckancontentmoderator.client.events.IFrameInstanciedEvent;
import org.gcube.portlets.widgets.ckancontentmoderator.client.events.IFrameInstanciedEvent.OPERATION;
import org.gcube.portlets.widgets.ckancontentmoderator.client.events.IFrameInstanciedEventHandler;
import org.gcube.portlets.widgets.ckancontentmoderator.client.events.SelectItemsWithItemStatusEvent;
import org.gcube.portlets.widgets.ckancontentmoderator.client.events.SelectItemsWithItemStatusEventHandler;
import org.gcube.portlets.widgets.ckancontentmoderator.client.events.ShowItemEvent;
import org.gcube.portlets.widgets.ckancontentmoderator.client.events.ShowItemEventHandler;
import org.gcube.portlets.widgets.ckancontentmoderator.client.events.ShowMessageEvent;
import org.gcube.portlets.widgets.ckancontentmoderator.client.events.ShowMessageEventHandler;
import org.gcube.portlets.widgets.ckancontentmoderator.client.events.TableRangeViewChangedEvent;
import org.gcube.portlets.widgets.ckancontentmoderator.client.events.TableRangeViewChangedEventHandler;
import org.gcube.portlets.widgets.ckancontentmoderator.client.ui.CkanShowItemFrame;
import org.gcube.portlets.widgets.ckancontentmoderator.client.ui.ContentModeratorToolbar;
import org.gcube.portlets.widgets.ckancontentmoderator.client.ui.HomeView;
import org.gcube.portlets.widgets.ckancontentmoderator.client.ui.MainTabPanel;
import org.gcube.portlets.widgets.ckancontentmoderator.client.ui.util.UtilUx;
import org.gcube.portlets.widgets.ckancontentmoderator.shared.CatalogueDataset;
import org.gcube.portlets.widgets.ckancontentmoderator.shared.DISPLAY_FIELD;
import org.gcube.portlets.widgets.ckancontentmoderator.shared.ItemFieldDV;
import org.gcube.portlets.widgets.ckancontentmoderator.shared.SearchingFilter;

import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.Scheduler;
import com.google.gwt.core.client.Scheduler.ScheduledCommand;
import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.ComplexPanel;
import com.google.gwt.user.client.ui.FlowPanel;

/**
 * The Class CkanContentModeratorWidgetController.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Jun 29, 2021
 */
public class CkanContentModeratorWidgetController {

	public final static CkanContentModeratorServiceAsync contentModeratorService = GWT
			.create(CkanContentModeratorService.class);

	private FlowPanel mainPanel = new FlowPanel();
	private FlowPanel infoPanel = new FlowPanel();

	private ContentModeratorToolbar toolbar;
	private MainTabPanel mainTabPanel = new MainTabPanel();

	public final static HandlerManager eventBus = new HandlerManager(null);
	private HomeView howeView;

	private HashMap<String, CkanShowItemFrame> mapOfItemsTabDisplayed = new HashMap<String, CkanShowItemFrame>();

	private boolean readOnlyMode;

	/**
	 * Instantiates a new ckan content moderator widget.
	 *
	 * @param status                     the status
	 * @param initOnItemName             the init on item name
	 * @param displayFields              the display fields
	 * @param sortByFields               the sort by fields
	 * @param readOnlyMode               the read only mode
	 * @param restrictDataToLoggedInUser the restrict data to logged in user
	 */
	protected CkanContentModeratorWidgetController(ItemStatus status, final String initOnItemName,
			DISPLAY_FIELD[] displayFields, DISPLAY_FIELD[] sortByFields, boolean readOnlyMode,
			boolean restrictDataToLoggedInUser) {

		DISPLAY_FIELD firstSortField = null;
		if (sortByFields == null || sortByFields.length == 0) {
			firstSortField = DISPLAY_FIELD.NAME;
			sortByFields[0] = firstSortField;
		} else
			firstSortField = sortByFields[0];

		List<ItemFieldDV> sortBy = displayFieldsToItemFields(sortByFields);

		toolbar = new ContentModeratorToolbar(eventBus, status, sortBy);

//		GWT.log("***************** HARD CODED READONLY ********************** ");
//		readOnlyMode = true;
//		restrictDataToLoggedInUser = readOnlyMode;

		howeView = new HomeView(eventBus, status, displayFields, firstSortField, readOnlyMode,
				restrictDataToLoggedInUser);
		mainTabPanel.addHomeWidget(howeView.getPanel());
		mainPanel.add(toolbar);
		mainPanel.add(infoPanel);
		mainPanel.add(mainTabPanel);
		this.readOnlyMode = readOnlyMode;

		howeView.hideUpdateStatusAction(readOnlyMode);
		howeView.hideSelectableRow(readOnlyMode);
		bindEvents();

		if (initOnItemName != null) {

			Scheduler.get().scheduleDeferred(new ScheduledCommand() {

				@Override
				public void execute() {

					CkanContentModeratorWidgetController.contentModeratorService.getItemForName(initOnItemName,
							new AsyncCallback<CatalogueDataset>() {

								@Override
								public void onSuccess(CatalogueDataset result) {
									GWT.log("CatalogueDataset read is: " + result);

									if (result != null) {
										eventBus.fireEvent(new ShowItemEvent<CatalogueDataset>(Arrays.asList(result),true));
										//Fired to set selectable row (i.e. checkbox) on STATUS (e.g. REJECT) 
										eventBus.fireEvent(new SelectItemsWithItemStatusEvent(status, null, null, null));
									}

								}

								@Override
								public void onFailure(Throwable caught) {
									GWT.log("Error on reading " + initOnItemName + ". Either the item with name: "
											+ initOnItemName + " is not valid dataset or not readable");

								}
							});
				}

			});

		}

	}

	/**
	 * TEMPORARY SOLUTION... IT SHOULD BE A CONFIGURATION
	 * 
	 * @param sortByFields
	 * @return
	 */

	private List<ItemFieldDV> displayFieldsToItemFields(DISPLAY_FIELD[] sortByFields) {

		List<ItemFieldDV> listSortByIF = new ArrayList<ItemFieldDV>(sortByFields.length);
		for (DISPLAY_FIELD display_FIELD : sortByFields) {
			for (SearchingFilter.ORDER order : SearchingFilter.ORDER.values()) {
				ItemFieldDV itemField = new ItemFieldDV();
				itemField.setDisplayName(display_FIELD.getLabel() + " - " + order);
				itemField.setJsonFields(Arrays.asList(display_FIELD.getJsonField() + " " + order.name().toLowerCase()));
				listSortByIF.add(itemField);
			}
		}

		return listSortByIF;
	}

	/**
	 * Bind events.
	 */
	private void bindEvents() {

		eventBus.addHandler(ClickItemEvent.TYPE, new ClickItemEventHandler() {

			@Override
			public <T> void onClick(ClickItemEvent<T> clickItemEvent) {

				if (clickItemEvent.getSelectItems() != null) {
					List<T> items = clickItemEvent.getSelectItems();
					// if items selected are > 0 then shows button "Update status as.."
					howeView.setVisibleUpdateStatusAction(items.size() > 0);

					if (howeView.getDisplayingItemStatus().equals(ItemStatus.APPROVED)) {
						GWT.log("The Item Status displayed is " + ItemStatus.APPROVED
								+ " hiding update status actions");
						howeView.setVisibleUpdateStatusAction(false);
					}

					// If an action updated the item status we reloading it
					reloadItemsDisplayedInTab();
				}
			}
		});

		eventBus.addHandler(ShowItemEvent.TYPE, new ShowItemEventHandler() {

			@Override
			public <T> void onShowItemClicked(ShowItemEvent<T> showItemEvent) {
				GWT.log("onShowItemClicked fired");
				if (showItemEvent.getSelectItems() != null) {
					List<T> items = showItemEvent.getSelectItems();

					for (T t : items) {
						CatalogueDataset clickedDataset = (CatalogueDataset) t;
						CkanShowItemFrame csif = new CkanShowItemFrame(eventBus);
						csif.instanceFrame(clickedDataset.getUrl());
						mainTabPanel.addTab(clickedDataset.getTitle(), csif, showItemEvent.isFocusOnDisplaying());
						mapOfItemsTabDisplayed.put(clickedDataset.getUrl(), csif);
					}
				}
			}
		});

		eventBus.addHandler(IFrameInstanciedEvent.TYPE, new IFrameInstanciedEventHandler() {

			@Override
			public void onIFrameAction(IFrameInstanciedEvent iFrameInstanciedEent) {

				OPERATION operation = iFrameInstanciedEent.getOperation();
				if (operation != null) {
					switch (operation) {
					case INIT:
						break;
					case ONLOAD:

						break;

					default:
						break;
					}
				}

			}
		});

		eventBus.addHandler(CheckBoxSelectIemsEvent.TYPE, new CheckBoxSelectIemsEventHandler() {

			@Override
			public <T> void onValueChanged(CheckBoxSelectIemsEvent<T> onValueChangedEvent) {
				howeView.markItemsAsChecked(onValueChangedEvent.isChecked(), true);

			}
		});

		eventBus.addHandler(TableRangeViewChangedEvent.TYPE, new TableRangeViewChangedEventHandler() {

			@Override
			public <T> void onRangeChanged(TableRangeViewChangedEvent<T> tableRangeViewChangedEvent) {
				howeView.setVisibleUpdateStatusAction(false);
				howeView.setCheckedCheckboxSelectAll(false);
			}
		});

		eventBus.addHandler(CloseAllTabsEvent.TYPE, new CloseAllTabsEventHandler() {

			@Override
			public void onClick(CloseAllTabsEvent closeAllTabsEvent) {
				mainTabPanel.closeTabs();
				mapOfItemsTabDisplayed.clear();
			}
		});

		eventBus.addHandler(SelectItemsWithItemStatusEvent.TYPE, new SelectItemsWithItemStatusEventHandler() {

			@Override
			public void onValueChanged(SelectItemsWithItemStatusEvent statusSelectedEvent) {
				GWT.log("On value changed: " + statusSelectedEvent.getItemStatus());
				infoPanel.clear();
				if (statusSelectedEvent.getItemStatus() != null) {
					ItemFieldDV sortBy = statusSelectedEvent.getSortBy();

					if (sortBy == null) {
						sortBy = toolbar.getActiveSortBy();

					}
					String sortForField = sortBy.getJsonFields().get(0);
					GWT.log("sortForField is: " + sortForField);
					howeView.loadItemsWithStatus(statusSelectedEvent.getItemStatus(), sortForField);
					mainTabPanel.selectTab(0);
					
					//#23903 Enabling the owner of the rejected item, to change the Status and send a message
					switch (statusSelectedEvent.getItemStatus()) {
					case REJECTED:
						howeView.hideUpdateStatusAction(false);
						howeView.hideSelectableRow(false);
						break;

					default:
						howeView.hideUpdateStatusAction(readOnlyMode);
						howeView.hideSelectableRow(readOnlyMode);
						break;
					}
					//#23903 end
					
				}

				if (statusSelectedEvent.getDisplayMessage() != null) {
					UtilUx.showAlert(statusSelectedEvent.getDisplayMessage(), statusSelectedEvent.getAlertType(), true,
							infoPanel, 15000);
				}

			}
		});

		eventBus.addHandler(ShowMessageEvent.TYPE, new ShowMessageEventHandler() {

			@Override
			public void onShowMessage(ShowMessageEvent showMessageEvent) {

				if (showMessageEvent.getMsg() != null) {
					UtilUx.showAlert(showMessageEvent.getMsg(), showMessageEvent.getAlerType(), true, infoPanel, 15000);

				}
			}
		});
	}

	/**
	 * Reload items displayed in tab.
	 */
	private void reloadItemsDisplayedInTab() {

		for (String datasetURL : mapOfItemsTabDisplayed.keySet()) {
			CkanShowItemFrame showItemFrame = mapOfItemsTabDisplayed.get(datasetURL);
			showItemFrame.reloadPage();
		}
	}

	/**
	 * Gets the main panel.
	 *
	 * @return the main panel
	 */
	public ComplexPanel getMainPanel() {
		return mainPanel;
	}

	/**
	 * Checks if is read only mode.
	 *
	 * @return true, if is read only mode
	 */
	public boolean isReadOnlyMode() {
		return readOnlyMode;
	}

}
