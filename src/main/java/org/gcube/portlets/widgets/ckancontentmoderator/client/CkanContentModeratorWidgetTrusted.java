package org.gcube.portlets.widgets.ckancontentmoderator.client;

import org.gcube.datacatalogue.utillibrary.shared.ItemStatus;
import org.gcube.portlets.widgets.ckancontentmoderator.client.ui.MainPanel;
import org.gcube.portlets.widgets.ckancontentmoderator.client.ui.util.ExtModal;
import org.gcube.portlets.widgets.ckancontentmoderator.shared.DISPLAY_FIELD;

import com.github.gwtbootstrap.client.ui.Modal;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.ui.Composite;

/**
 * The Class CkanContentModeratorWidget.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Jun 15, 2021
 */
public class CkanContentModeratorWidgetTrusted {

	private MainPanel mainPanel = new MainPanel();

	private CheckConfigsUxController ccux;

	/**
	 * Instantiates a new ckan content moderator widget trusted.
	 *
	 * @param initItemStatus the init item status
	 * @param displayFields                   the display fields
	 * @param sortByField                     the sort by field
	 * @param ckanContentModeratorCheckConfig the ckan content moderator check
	 *                                        config
	 */
	public CkanContentModeratorWidgetTrusted(ItemStatus initItemStatus, DISPLAY_FIELD[] displayFields,
			DISPLAY_FIELD[] sortByFields, CkanContentModeratorCheckConfigs ckanContentModeratorCheckConfig) {
		GWT.log("CkanContentModeratorWidget called. CkanContentModeratorCheckConfigs: "
				+ ckanContentModeratorCheckConfig);
		mainPanel.setWidth("100%");
		ccux = new CheckConfigsUxController(mainPanel, initItemStatus, null, displayFields, sortByFields, ckanContentModeratorCheckConfig);
	}

	/**
	 * Show as modal.
	 *
	 * @param modalTitle the modal title
	 */
	public void showAsModal(String modalTitle) {
		final Modal modal = new ExtModal(true, true);
		modalTitle = modalTitle == null || modalTitle.isEmpty() ? "Manage Items" : modalTitle;
		modal.setTitle(modalTitle);
		modal.setWidth(1200);
		modal.getElement().addClassName("modal-content-moderator");
		// modal.getElement().getStyle().setProperty("min-height", "500px");
		modal.add(mainPanel);
		modal.setCloseVisible(true);
		modal.show();
	}

	/**
	 * Gets the panel.
	 *
	 * @return the panel
	 */
	public Composite getPanel() {
		return mainPanel;
	}

	/**
	 * Gets the configs.
	 *
	 * @return the configs
	 */
	public CkanContentModeratorCheckConfigs getConfigs() {
		return ccux.getModeratorCheckConfig();
	}
}
