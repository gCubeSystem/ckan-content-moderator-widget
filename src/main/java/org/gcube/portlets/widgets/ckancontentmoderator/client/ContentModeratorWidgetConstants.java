package org.gcube.portlets.widgets.ckancontentmoderator.client;

import org.gcube.portlets.widgets.ckancontentmoderator.shared.DISPLAY_FIELD;

/**
 * The Class ContentModeratorWidgetConstants.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Jun 15, 2021
 */
public class ContentModeratorWidgetConstants {

	public static final String NO_DATA = "No data";

	public static int ITEMS_PER_PAGE = 10;
	public static int ITEM_START_INDEX = 0;

	public static String CKAN_FIELD_NAME_AUTHOR_MAIL = "author_email";

	public static DISPLAY_FIELD[] DEFAULT_SORT_BY_FIELDS = new DISPLAY_FIELD[] { DISPLAY_FIELD.NAME, DISPLAY_FIELD.TITLE,
			DISPLAY_FIELD.CREATED, DISPLAY_FIELD.LAST_UPDATE, DISPLAY_FIELD.TYPE };
}
