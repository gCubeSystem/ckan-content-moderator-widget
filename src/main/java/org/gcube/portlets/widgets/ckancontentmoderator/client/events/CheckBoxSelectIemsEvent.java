package org.gcube.portlets.widgets.ckancontentmoderator.client.events;

import com.google.gwt.event.shared.GwtEvent;

/**
 * The Class CheckBoxSelectIemsEvent.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Jun 22, 2021
 * @param <T> the generic type
 */
public class CheckBoxSelectIemsEvent<T> extends GwtEvent<CheckBoxSelectIemsEventHandler> {
	public static Type<CheckBoxSelectIemsEventHandler> TYPE = new Type<CheckBoxSelectIemsEventHandler>();
	private boolean isChecked;

	/**
	 * Instantiates a new click item event.
	 *
	 * @param selectItems the select items
	 */
	public CheckBoxSelectIemsEvent(boolean checkedValue) {
		this.isChecked = checkedValue;
	}

	/**
	 * Gets the associated type.
	 *
	 * @return the associated type
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see com.google.gwt.event.shared.GwtEvent#getAssociatedType()
	 */
	@Override
	public Type<CheckBoxSelectIemsEventHandler> getAssociatedType() {
		return TYPE;
	}

	/**
	 * Dispatch.
	 *
	 * @param handler the handler
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.google.gwt.event.shared.GwtEvent#dispatch(com.google.gwt.event.shared.
	 * EventHandler)
	 */
	@Override
	protected void dispatch(CheckBoxSelectIemsEventHandler handler) {
		handler.onValueChanged(this);
	}
	
	public boolean isChecked() {
		return isChecked;
	}

}
