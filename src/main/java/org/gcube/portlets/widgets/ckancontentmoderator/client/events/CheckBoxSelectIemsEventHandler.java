package org.gcube.portlets.widgets.ckancontentmoderator.client.events;

import com.google.gwt.event.shared.EventHandler;


/**
 * The Interface CheckBoxSelectIemsEventHandler.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 * Jun 22, 2021
 */
public interface CheckBoxSelectIemsEventHandler extends EventHandler {


	/**
	 * On value changed.
	 *
	 * @param <T> the generic type
	 * @param onValueChangedEvent the on value changed event
	 */
	<T> void onValueChanged(CheckBoxSelectIemsEvent<T> onValueChangedEvent);
}