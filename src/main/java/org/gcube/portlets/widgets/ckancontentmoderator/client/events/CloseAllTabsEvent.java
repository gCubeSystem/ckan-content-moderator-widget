package org.gcube.portlets.widgets.ckancontentmoderator.client.events;

import com.google.gwt.event.shared.GwtEvent;



// TODO: Auto-generated Javadoc
/**
 * The Class CloseAllTabsEvent.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 * Jun 22, 2021
 */
public class CloseAllTabsEvent extends GwtEvent<CloseAllTabsEventHandler> {
	public static Type<CloseAllTabsEventHandler> TYPE = new Type<CloseAllTabsEventHandler>();

	/**
	 * Instantiates a new close all tabs event.
	 */
	public CloseAllTabsEvent() {

	}

	/**
	 * Gets the associated type.
	 *
	 * @return the associated type
	 */
	@Override
	public Type<CloseAllTabsEventHandler> getAssociatedType() {
		return TYPE;
	}

	/**
	 * Dispatch.
	 *
	 * @param handler the handler
	 */
	@Override
	protected void dispatch(CloseAllTabsEventHandler handler) {
		handler.onClick(this);
	}

}
