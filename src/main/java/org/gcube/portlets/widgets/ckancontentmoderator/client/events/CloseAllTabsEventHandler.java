package org.gcube.portlets.widgets.ckancontentmoderator.client.events;

import com.google.gwt.event.shared.EventHandler;


/**
 * The Interface CloseAllTabsEventHandler.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Jun 22, 2021
 */
public interface CloseAllTabsEventHandler extends EventHandler {

	/**
	 * On click.
	 *
	 * @param closeAllTabsEvent the close all tabs event
	 */
	void onClick(CloseAllTabsEvent closeAllTabsEvent);
}