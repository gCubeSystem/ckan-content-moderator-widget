package org.gcube.portlets.widgets.ckancontentmoderator.client.events;

import com.google.gwt.event.shared.GwtEvent;
import com.google.gwt.user.client.ui.Frame;


/**
 * The Class IFrameInstanciedEvent.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 * Jun 28, 2021
 */
public class IFrameInstanciedEvent extends GwtEvent<IFrameInstanciedEventHandler> {
	public static Type<IFrameInstanciedEventHandler> TYPE = new Type<IFrameInstanciedEventHandler>();
	private Frame iFrame;
	private OPERATION operation;

	/**
	 * The Enum OPERATION.
	 *
	 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
	 * 
	 * Jun 28, 2021
	 */
	public static enum OPERATION {
		INIT, ONLOAD
	}

	/**
	 * Instantiates a new insert metadata event.
	 *
	 * @param iFrame the i frame
	 * @param operation the operation
	 */
	public IFrameInstanciedEvent(Frame iFrame, OPERATION operation) {
		this.iFrame = iFrame;
		this.operation = operation;
	}

	/**
	 * Gets the associated type.
	 *
	 * @return the associated type
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see com.google.gwt.event.shared.GwtEvent#getAssociatedType()
	 */
	@Override
	public Type<IFrameInstanciedEventHandler> getAssociatedType() {
		return TYPE;
	}

	/**
	 * Dispatch.
	 *
	 * @param handler the handler
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.google.gwt.event.shared.GwtEvent#dispatch(com.google.gwt.event.shared.
	 * EventHandler)
	 */
	@Override
	protected void dispatch(IFrameInstanciedEventHandler handler) {
		handler.onIFrameAction(this);
	}

	/**
	 * Gets the i frame.
	 *
	 * @return the i frame
	 */
	public Frame getiFrame() {
		return iFrame;
	}
	
	/**
	 * Gets the operation.
	 *
	 * @return the operation
	 */
	public OPERATION getOperation() {
		return operation;
	}

}
