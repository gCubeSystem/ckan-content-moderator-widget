package org.gcube.portlets.widgets.ckancontentmoderator.client.events;

import com.google.gwt.event.shared.EventHandler;


/**
 * The Interface IFrameInstanciedEventHandler.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 * Jun 28, 2021
 */
public interface IFrameInstanciedEventHandler extends EventHandler {

	
	/**
	 * On I frame action.
	 *
	 * @param iFrameInstanciedEent the i frame instancied eent
	 */
	void onIFrameAction(IFrameInstanciedEvent iFrameInstanciedEent);

}