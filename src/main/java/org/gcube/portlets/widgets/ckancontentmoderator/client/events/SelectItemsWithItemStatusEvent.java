package org.gcube.portlets.widgets.ckancontentmoderator.client.events;

import org.gcube.datacatalogue.utillibrary.shared.ItemStatus;
import org.gcube.portlets.widgets.ckancontentmoderator.shared.ItemFieldDV;

import com.github.gwtbootstrap.client.ui.constants.AlertType;
import com.google.gwt.event.shared.GwtEvent;

/**
 * The Class SelectItemsWithItemStatusEvent.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Jun 28, 2021
 */
public class SelectItemsWithItemStatusEvent extends GwtEvent<SelectItemsWithItemStatusEventHandler> {
	public static Type<SelectItemsWithItemStatusEventHandler> TYPE = new Type<SelectItemsWithItemStatusEventHandler>();
	private ItemStatus itemStatus;
	private String displayMessage;
	private AlertType alertType = AlertType.INFO;
	private ItemFieldDV sortBy;

	/**
	 * Instantiates a new click item event.
	 *
	 * @param itemStatus     the item status
	 * @param displayMessage the display message
	 * @param alerType       the aler type
	 * @param sortBy         the sort by
	 */
	public SelectItemsWithItemStatusEvent(ItemStatus itemStatus, String displayMessage, AlertType alerType,
			ItemFieldDV sortBy) {
		this.itemStatus = itemStatus;
		this.displayMessage = displayMessage;
		this.alertType = alerType;
		this.sortBy = sortBy;
	}

	/**
	 * Instantiates a new select items with item status event.
	 *
	 * @param itemStatus     the item status
	 * @param displayMessage the display message
	 * @param alerType       the aler type
	 */
	public SelectItemsWithItemStatusEvent(ItemStatus itemStatus, String displayMessage, AlertType alerType) {
		this.itemStatus = itemStatus;
		this.displayMessage = displayMessage;
		this.alertType = alerType;
	}

	/**
	 * Gets the associated type.
	 *
	 * @return the associated type
	 */
	@Override
	public Type<SelectItemsWithItemStatusEventHandler> getAssociatedType() {
		return TYPE;
	}

	/**
	 * Dispatch.
	 *
	 * @param handler the handler
	 */
	@Override
	protected void dispatch(SelectItemsWithItemStatusEventHandler handler) {
		handler.onValueChanged(this);
	}

	/**
	 * Gets the item status.
	 *
	 * @return the item status
	 */
	public ItemStatus getItemStatus() {
		return itemStatus;
	}

	/**
	 * Gets the display message.
	 *
	 * @return the display message
	 */
	public String getDisplayMessage() {
		return displayMessage;
	}

	/**
	 * Gets the alert type.
	 *
	 * @return the alert type
	 */
	public AlertType getAlertType() {
		return alertType;
	}

	/**
	 * Gets the sort by.
	 *
	 * @return the sort by
	 */
	public ItemFieldDV getSortBy() {
		return sortBy;
	}

}
