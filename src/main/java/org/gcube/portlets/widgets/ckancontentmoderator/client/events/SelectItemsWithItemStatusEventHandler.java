package org.gcube.portlets.widgets.ckancontentmoderator.client.events;

import com.google.gwt.event.shared.EventHandler;


/**
 * The Interface SelectItemsWithItemStatusEventHandler.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 * Jun 28, 2021
 */
public interface SelectItemsWithItemStatusEventHandler extends EventHandler {

	/**
	 * On value changed.
	 *
	 * @param statusSelectedEvent the status selected event
	 */
	void onValueChanged(SelectItemsWithItemStatusEvent statusSelectedEvent);
}