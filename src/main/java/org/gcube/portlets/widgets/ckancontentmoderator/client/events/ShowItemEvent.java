package org.gcube.portlets.widgets.ckancontentmoderator.client.events;

import java.util.List;

import com.google.gwt.event.shared.GwtEvent;

/**
 * The Class ShowItemEvent.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Jun 22, 2021
 * @param <T> the generic type
 */
public class ShowItemEvent<T> extends GwtEvent<ShowItemEventHandler> {
	public static Type<ShowItemEventHandler> TYPE = new Type<ShowItemEventHandler>();
	private List<T> selectItems;
	private boolean focusOnDisplaying = false;

	/**
	 * Instantiates a new click item event.
	 *
	 * @param selectItems the select items
	 * @param focusOnDisplaying the focus on displaying
	 */
	public ShowItemEvent(List<T> selectItems, boolean focusOnDisplaying) {
		this.selectItems = selectItems;
		this.focusOnDisplaying = focusOnDisplaying;
	}

	/**
	 * Gets the associated type.
	 *
	 * @return the associated type
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see com.google.gwt.event.shared.GwtEvent#getAssociatedType()
	 */
	@Override
	public Type<ShowItemEventHandler> getAssociatedType() {
		return TYPE;
	}

	/**
	 * Dispatch.
	 *
	 * @param handler the handler
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.google.gwt.event.shared.GwtEvent#dispatch(com.google.gwt.event.shared.
	 * EventHandler)
	 */
	@Override
	protected void dispatch(ShowItemEventHandler handler) {
		handler.onShowItemClicked(this);
	}

	/**
	 * Gets the select items.
	 *
	 * @return the select items
	 */
	public List<T> getSelectItems() {
		return selectItems;
	}
	
	/**
	 * Checks if is focus on displaying.
	 *
	 * @return true, if is focus on displaying
	 */
	public boolean isFocusOnDisplaying() {
		return focusOnDisplaying;
	}
}
