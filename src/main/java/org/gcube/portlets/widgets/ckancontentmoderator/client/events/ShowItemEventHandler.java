package org.gcube.portlets.widgets.ckancontentmoderator.client.events;

import com.google.gwt.event.shared.EventHandler;


// TODO: Auto-generated Javadoc
/**
 * The Interface ShowItemEventHandler.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 * Jun 22, 2021
 */
public interface ShowItemEventHandler extends EventHandler {

	/**
	 * On show item clicked.
	 *
	 * @param <T> the generic type
	 * @param showItemEvent the show item event
	 */
	<T> void onShowItemClicked(ShowItemEvent<T> showItemEvent);
}