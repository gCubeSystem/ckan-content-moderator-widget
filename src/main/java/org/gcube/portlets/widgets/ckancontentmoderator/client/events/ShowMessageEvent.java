package org.gcube.portlets.widgets.ckancontentmoderator.client.events;

import com.github.gwtbootstrap.client.ui.constants.AlertType;
import com.google.gwt.event.shared.GwtEvent;

/**
 * The Class ShowMessageEvent.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 * Feb 22, 2022
 */
public class ShowMessageEvent extends GwtEvent<ShowMessageEventHandler> {
	public static Type<ShowMessageEventHandler> TYPE = new Type<ShowMessageEventHandler>();
	private String msg;
	private AlertType alerType;

	/**
	 * Instantiates a new click item event.
	 *
	 * @param msg the msg
	 * @param alerType the aler type
	 */
	public ShowMessageEvent(String msg, AlertType alerType) {
		this.msg = msg;
		this.alerType = alerType;
	}

	/**
	 * Gets the associated type.
	 *
	 * @return the associated type
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see com.google.gwt.event.shared.GwtEvent#getAssociatedType()
	 */
	@Override
	public Type<ShowMessageEventHandler> getAssociatedType() {
		return TYPE;
	}

	/**
	 * Dispatch.
	 *
	 * @param handler the handler
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.google.gwt.event.shared.GwtEvent#dispatch(com.google.gwt.event.shared.
	 * EventHandler)
	 */
	@Override
	protected void dispatch(ShowMessageEventHandler handler) {
		handler.onShowMessage(this);
	}
	
	public String getMsg() {
		return msg;
	}
	
	public AlertType getAlerType() {
		return alerType;
	}
}
