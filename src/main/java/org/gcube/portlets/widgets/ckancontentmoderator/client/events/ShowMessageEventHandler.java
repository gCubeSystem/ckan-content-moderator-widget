package org.gcube.portlets.widgets.ckancontentmoderator.client.events;

import com.google.gwt.event.shared.EventHandler;

/**
 * The Interface ShowMessageEventHandler.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 * Feb 22, 2022
 */
public interface ShowMessageEventHandler extends EventHandler {

	/**
	 * On show message.
	 *
	 * @param showMessageEvent the show message event
	 */
	void onShowMessage(ShowMessageEvent showMessageEvent);
}