package org.gcube.portlets.widgets.ckancontentmoderator.client.events;

import com.google.gwt.event.shared.GwtEvent;

/**
 * The Class TableRangeViewChangedEvent.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Jun 22, 2021
 * @param <T> the generic type
 */
public class TableRangeViewChangedEvent<T> extends GwtEvent<TableRangeViewChangedEventHandler> {
	public static Type<TableRangeViewChangedEventHandler> TYPE = new Type<TableRangeViewChangedEventHandler>();
	private int startIndex;
	private int limitIndex;

	/**
	 * Instantiates a new click item event.
	 *
	 * @param handler the handler
	 * @return 
	 */
	public TableRangeViewChangedEvent(int start, int limit) {
		this.startIndex = start;
		this.limitIndex = limit;
	}

	@Override
	public Type<TableRangeViewChangedEventHandler> getAssociatedType() {
		return TYPE;
	}

	@Override
	protected void dispatch(TableRangeViewChangedEventHandler handler) {
		handler.onRangeChanged(this);
	}
	
	public int getStartIndex() {
		return startIndex;
	}
	
	public int getLimitIndex() {
		return limitIndex;
	}

}
