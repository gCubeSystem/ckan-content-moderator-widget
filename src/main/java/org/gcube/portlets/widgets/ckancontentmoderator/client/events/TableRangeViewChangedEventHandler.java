package org.gcube.portlets.widgets.ckancontentmoderator.client.events;

import com.google.gwt.event.shared.EventHandler;

/**
 * The Interface TableRangeViewChangedEventHandler.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 * Jun 22, 2021
 */
public interface TableRangeViewChangedEventHandler extends EventHandler {

	<T> void onRangeChanged(TableRangeViewChangedEvent<T> tableRangeViewChangedEvent);

}