package org.gcube.portlets.widgets.ckancontentmoderator.client.resources;

import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.ImageResource;

/**
 * The Interface WorkspaceExplorerIcons.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 * Jun 15, 2021
 */
public interface ContentModeratorIcons extends ClientBundle {

	/**
	 * Cancel.
	 *
	 * @return the image resource
	 */
	@Source("cancel.png")
	ImageResource cancel();

	/**
	 * Loading.
	 *
	 * @return the image resource
	 */
	@Source("loading.gif")
	ImageResource loading();

	/**
	 * Our sample image icon. Makes the image resource for the gwt-compiler's css
	 * composer accessible
	 *
	 * @return the image resource
	 */
	@Source("loading.gif")
	ImageResource logo();

	/**
	 * Info.
	 *
	 * @return the image resource
	 */
	@Source("info-icon.png")
	ImageResource info();

	/**
	 * Info square.
	 *
	 * @return the image resource
	 */

	@Source("info-square.png")
	ImageResource infoSquare();
}
