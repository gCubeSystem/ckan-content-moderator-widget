package org.gcube.portlets.widgets.ckancontentmoderator.client.resources;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.ui.AbstractImagePrototype;

/**
 * The Class ContentModeratorWidgetResources.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Jun 15, 2021
 */
public class ContentModeratorWidgetResources {

	public static final ContentModeratorIcons ICONS = GWT.create(ContentModeratorIcons.class);

	/**
	 * Gets the icon cancel.
	 *
	 * @return the icon cancel
	 */
	public static AbstractImagePrototype getIconCancel() {

		return AbstractImagePrototype.create(ICONS.cancel());
	}

	/**
	 * Gets the icon loading.
	 *
	 * @return the icon loading
	 */
	public static AbstractImagePrototype getIconLoading() {

		return AbstractImagePrototype.create(ICONS.loading());
	}

	/**
	 * Gets the icon info.
	 *
	 * @return the icon info
	 */
	public static AbstractImagePrototype getIconInfo() {

		return AbstractImagePrototype.create(ICONS.info());
	}

	/**
	 * Gets the icon info square.
	 *
	 * @return the icon info square
	 */
	public static AbstractImagePrototype getIconInfoSquare() {

		return AbstractImagePrototype.create(ICONS.infoSquare());
	}
}
