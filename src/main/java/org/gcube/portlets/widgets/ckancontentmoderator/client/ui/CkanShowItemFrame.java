package org.gcube.portlets.widgets.ckancontentmoderator.client.ui;

import org.gcube.portlets.widgets.ckancontentmoderator.client.events.IFrameInstanciedEvent;
import org.gcube.portlets.widgets.ckancontentmoderator.client.events.IFrameInstanciedEvent.OPERATION;
import org.gcube.portlets.widgets.ckancontentmoderator.client.resources.ContentModeratorWidgetResources;

import com.github.gwtbootstrap.client.ui.Button;
import com.github.gwtbootstrap.client.ui.constants.IconType;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style.Display;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.LoadEvent;
import com.google.gwt.event.dom.client.LoadHandler;
import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.http.client.URL;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.Random;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Frame;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.NamedFrame;
import com.google.gwt.user.client.ui.Widget;

/**
 * The Class CkanShowItemFrame.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Apr 4, 2022
 */
public class CkanShowItemFrame extends Composite {

	private static CkanShowItemFrameUiBinder uiBinder = GWT.create(CkanShowItemFrameUiBinder.class);

	private NamedFrame namedFrame;
	private HandlerManager eventBus;
	private Image loading = new Image(ContentModeratorWidgetResources.ICONS.loading());
	private String messageToSend;
	private String iFrameRandomName;

	@UiField
	Button button_reload;

	@UiField
	HTMLPanel panel_container;

//	@UiField
//	AlertBlock alertBlockNav;

	private String datasetURL;

	/**
	 * The Interface CkanShowItemFrameUiBinder.
	 *
	 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
	 * 
	 *         Apr 4, 2022
	 */
	interface CkanShowItemFrameUiBinder extends UiBinder<Widget, CkanShowItemFrame> {
	}

	/**
	 * Instantiates a new ckan show item frame.
	 *
	 * @param eventBus the event bus
	 */
	public CkanShowItemFrame(HandlerManager eventBus) {
		initWidget(uiBinder.createAndBindUi(this));
		this.eventBus = eventBus;
//		alertBlockNav.setType(AlertType.INFO);
		button_reload.setIcon(IconType.ROTATE_RIGHT);
		button_reload.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				reloadPage();
			}
		});
	}

	/**
	 * Instance frame.
	 *
	 * @param datasetURL the dataset URL
	 * @return the frame
	 */
	public Frame instanceFrame(String datasetURL) {
		this.datasetURL = datasetURL;
		GWT.log("Instancing new IFRAME with uri: " + datasetURL);
		addLoading();
		String urlEncoded = URL.encode(datasetURL);
		GWT.log("Encoded url for instanciating namedFrame is " + urlEncoded);
		iFrameRandomName = Random.nextInt() + "dataset-internal-iframe" + Random.nextInt();
		namedFrame = new NamedFrame(iFrameRandomName);
		namedFrame.setUrl(urlEncoded);

		namedFrame.getElement().setId(iFrameRandomName);
		namedFrame.setWidth("100%");
		namedFrame.setHeight("580px");
		namedFrame.getElement().getStyle().setBorderWidth(0, Unit.PX);
		namedFrame.addLoadHandler(new LoadHandler() {

			@Override
			public void onLoad(LoadEvent arg0) {

				removeLoading();
				eventBus.fireEvent(new IFrameInstanciedEvent(namedFrame, OPERATION.ONLOAD));
			}
		});
		panel_container.add(namedFrame);
		namedFrame.setVisible(true);
		eventBus.fireEvent(new IFrameInstanciedEvent(namedFrame, OPERATION.INIT));
		return namedFrame;

	}

	/**
	 * Gets the namedFrame.
	 *
	 * @return the namedFrame
	 */
	public Frame getFrame() {
		return namedFrame;
	}

	/**
	 * add loading image.
	 */
	private void addLoading() {
		panel_container.add(loading);
		loading.getElement().getStyle().setProperty("margin", "auto");
		loading.getElement().getStyle().setDisplay(Display.BLOCK);
		button_reload.setEnabled(false);
	}

	/**
	 * Removes the loading.
	 */
	private void removeLoading() {
		try {
			panel_container.remove(loading);
			button_reload.setEnabled(true);
		} catch (Exception e) {

		}
	}

	/**
	 * Reload page.
	 */
	public void reloadPage() {
		panel_container.clear();
		instanceFrame(datasetURL);
	}

}
