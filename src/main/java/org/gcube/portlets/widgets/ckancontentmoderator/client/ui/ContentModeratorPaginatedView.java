package org.gcube.portlets.widgets.ckancontentmoderator.client.ui;

import java.util.List;

import org.gcube.datacatalogue.utillibrary.shared.ItemStatus;
import org.gcube.portlets.widgets.ckancontentmoderator.client.CkanContentModeratorWidgetController;
import org.gcube.portlets.widgets.ckancontentmoderator.client.ContentModeratorWidgetConstants;
import org.gcube.portlets.widgets.ckancontentmoderator.client.events.ShowMessageEvent;
import org.gcube.portlets.widgets.ckancontentmoderator.client.events.TableRangeViewChangedEvent;
import org.gcube.portlets.widgets.ckancontentmoderator.client.ui.table.ItemsTable;
import org.gcube.portlets.widgets.ckancontentmoderator.client.ui.table.SortedCellTable;
import org.gcube.portlets.widgets.ckancontentmoderator.client.ui.util.LoadingPanel;
import org.gcube.portlets.widgets.ckancontentmoderator.shared.CatalogueDataset;
import org.gcube.portlets.widgets.ckancontentmoderator.shared.DISPLAY_FIELD;
import org.gcube.portlets.widgets.ckancontentmoderator.shared.SearchedData;

import com.github.gwtbootstrap.client.ui.constants.AlertType;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.user.cellview.client.SimplePager;
import com.google.gwt.user.cellview.client.SimplePager.TextLocation;
import com.google.gwt.user.client.Element;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.view.client.AsyncDataProvider;
import com.google.gwt.view.client.HasData;
import com.google.gwt.view.client.MultiSelectionModel;
import com.google.gwt.view.client.Range;
import com.google.gwt.view.client.SelectionModel;
import com.google.gwt.view.client.SingleSelectionModel;

/**
 * The Class GeonaRecordsPaginatedView.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Jun 16, 2021
 */
public class ContentModeratorPaginatedView {

	private static final int ITEM_START_INDEX = ContentModeratorWidgetConstants.ITEM_START_INDEX;
	private static final int ITEMS_PER_PAGE = ContentModeratorWidgetConstants.ITEMS_PER_PAGE;
	private VerticalPanel vPanel = new VerticalPanel();
	private FlowPanel pagerPanel = new FlowPanel();
	private Boolean initClassFirstRangeChanged = false;
	private ItemsTable<CatalogueDataset> itemsTable;
	private MyCustomDataProvider<CatalogueDataset> dataProvider = new MyCustomDataProvider<CatalogueDataset>();
	protected Widget orginalLoadingIndicator = null;
	private LoadingPanel loadingPanel = new LoadingPanel(new HTML("Loading data..."));
	private ItemStatus itemStatus;
	private int serverStartIndex;
	private HandlerManager eventBus;
	private boolean restrictDataToLoggedInUser;
	private String sortForField;

	/**
	 * Instantiates a new content moderator paginated view.
	 *
	 * @param eventbus                   the eventbus
	 * @param theStatus                  the the status
	 * @param displayFields              the display fields
	 * @param sortByField                the sort by field
	 * @param restrictDataToLoggedInUser the restrict data to logged in user
	 * @param sortForField
	 */
	public ContentModeratorPaginatedView(HandlerManager eventbus, ItemStatus theStatus, DISPLAY_FIELD[] displayFields,
			DISPLAY_FIELD sortByField, boolean restrictDataToLoggedInUser) {
		this.itemStatus = theStatus;
		this.restrictDataToLoggedInUser = restrictDataToLoggedInUser;
		this.initClassFirstRangeChanged = true;
		this.eventBus = eventbus;
		itemsTable = new ItemsTable<CatalogueDataset>(eventbus, displayFields, sortByField);
		itemsTable.initTable(null, null, dataProvider);

		orginalLoadingIndicator = itemsTable.getCellTable().getLoadingIndicator();
		initPagination(ITEMS_PER_PAGE);
		// loadNewPage(ITEM_START_INDEX, ITEMS_PER_PAGE, false);
		loadItemsForStatus(theStatus, sortForField);

	}

	/**
	 * Gets the asycn data provider.
	 *
	 * @return the asycn data provider
	 */
	public AsyncDataProvider<CatalogueDataset> getAsycnDataProvider() {
		return (AsyncDataProvider<CatalogueDataset>) getCellTable().getDataProvider();
	}

	/**
	 * Gets the cell tale.
	 *
	 * @return the cell tale
	 */
	private SortedCellTable<CatalogueDataset> getCellTable() {
		return itemsTable.getCellTable();
	}

	/**
	 * Inits the pagination.
	 *
	 * @param itemsPerPage the items per page
	 */
	public void initPagination(int itemsPerPage) {

		SimplePager.Resources pagerResources = GWT.create(SimplePager.Resources.class);
		SimplePager pager = new SimplePager(TextLocation.CENTER, pagerResources, false, 0, true);
		pager.setDisplay(getCellTable());
		pager.setPageSize(itemsPerPage);
		pager.getElement().getStyle().setProperty("margin", "auto");
		vPanel.add(loadingPanel);

		ScrollPanel scroll = new ScrollPanel();
		scroll.getElement().getStyle().setProperty("maxHeight", "470px");
		scroll.add(getCellTable());
		vPanel.add(scroll);
		vPanel.getElement().addClassName("vPanel");
		pagerPanel.add(pager);
	}

	/**
	 * Gets the pager panel.
	 *
	 * @return the pager panel
	 */
	public VerticalPanel getCellPanel() {
		return vPanel;
	}

	/**
	 * Set the panel in loading mode.
	 *
	 * @param show the show
	 */
	protected void showLoading(boolean show) {
		loadingPanel.setVisible(show);
	}

	/**
	 * Gets the pager panel.
	 *
	 * @return the pager panel
	 */
	public FlowPanel getPagerPanel() {
		return pagerPanel;
	}

	/**
	 * Load new page.
	 *
	 * @param startIdx     the start idx
	 * @param limit        the limit
	 * @param resetStore   the reset store
	 * @param sortForField
	 */
	private void loadNewPage(final int startIdx, final int limit, final boolean resetStore) {
		// initFirstRangeChanged = resetStore;
		GWT.log("loadNewPage with parameters [startIdx: " + startIdx + ", limit: " + limit + ", resetStore:"
				+ resetStore + "]");
		// showLoading(true);

		int newStartIndex = startIdx;

		if (resetStore) {
			GWT.log("Cleaning all data...");
			newStartIndex = 0;
			serverStartIndex = 0;
			GWT.log("Store reset performed start index is: " + newStartIndex);
			getAsycnDataProvider().updateRowCount(ITEMS_PER_PAGE, false);
		}

		loadItemsForStatus(itemStatus, newStartIndex, limit, serverStartIndex, sortForField);

	}

	/**
	 * Load items for status.
	 *
	 * @param itemStatus   the item status
	 * @param sortForField
	 */
	public void loadItemsForStatus(ItemStatus itemStatus, String sortForField) {
		this.itemStatus = itemStatus;
		this.sortForField = sortForField;
		getCellTable().setVisibleRangeAndClearData(new Range(0, ITEMS_PER_PAGE), false);
		loadNewPage(ITEM_START_INDEX, ITEMS_PER_PAGE, true);
	}

	/**
	 * Sets the new page result.
	 *
	 * @param result the new new page result
	 */
	private void setNewPageResult(SearchedData result) {
		GWT.log("setNewPageResult: " + result);
		
		if(!GWT.isProdMode()) {
			if(result.getData()!=null) {
				for (CatalogueDataset dataset : result.getData()) {
					GWT.log("dataset: " + dataset.getStatus());
				}
			}
		}
		
		serverStartIndex = result.getServerEndIndex();
		SelectionModel<? super CatalogueDataset> sm = getCellTable().getSelectionModel();

		if (sm instanceof SingleSelectionModel) {
			SingleSelectionModel<CatalogueDataset> ssm = (SingleSelectionModel<CatalogueDataset>) sm;
			ssm.clear();
		} else if (sm instanceof MultiSelectionModel) {
			MultiSelectionModel<CatalogueDataset> msm = (MultiSelectionModel<CatalogueDataset>) sm;
			msm.clear();
		}

		getAsycnDataProvider().updateRowCount((int) result.getTotalItems(), true);
		getAsycnDataProvider().updateRowData(result.getClientStartIndex(), result.getData());

		/*
		 * MOCK-UP int total = 35; List<CatalogueDataset> listData = new
		 * ArrayList<CatalogueDataset>(35); for (int i = 0; i < total; i++) {
		 * CatalogueDataset cd = new CatalogueDataset(); cd.setId(i+"");
		 * cd.setName("name "+i);
		 * 
		 * listData.add(cd);
		 * 
		 * }
		 * 
		 * getAsycnDataProvider().updateRowCount(total, true);
		 * getAsycnDataProvider().updateRowData(result.getClientStartIndex(), listData);
		 */

		if (result.getData().size() == 0) {
			getCellTable().setLoadingIndicator(new Label("No data"));
		} else {
			getCellTable().setLoadingIndicator(orginalLoadingIndicator);
		}

		GWT.log("Updating row data startIndex: " + result.getClientStartIndex() + " children size: "
				+ result.getData().size());
		GWT.log("getAsycnDataProvider().getDataDisplays().size(): " + getCellTable().getRowCount());

		if (result.isServerSearchFinished()) {
			GWT.log("Search finished!!!");
			getAsycnDataProvider().updateRowCount(getCellTable().getRowCount(), true);
		}
		// initFirstRangeChanged = false;

	}

	/**
	 * Load items for status.
	 *
	 * @param status       the status
	 * @param offset       the offset
	 * @param limit        the limit
	 * @param serverIndex  the server index
	 * @param sortForField
	 */
	private void loadItemsForStatus(ItemStatus status, int offset, int limit, int serverIndex, String sortForField) {
		showLoading(true);

		GWT.log("calling getDataForStatus with parameters [startIndex: " + offset + ", limit: " + limit
				+ ", serverIndex:" + serverIndex + "]");

		CkanContentModeratorWidgetController.contentModeratorService.getDataForStatus(status, offset, limit,
				serverIndex, restrictDataToLoggedInUser, sortForField, new AsyncCallback<SearchedData>() {

					@Override
					public void onSuccess(SearchedData result) {
						showLoading(false);
						setNewPageResult(result);
					}

					@Override
					public void onFailure(Throwable caught) {
						showLoading(false);
						eventBus.fireEvent(new ShowMessageEvent(caught.getMessage(), AlertType.ERROR));

					}
				});

	}

	/**
	 * Select items.
	 *
	 * @param select      the select
	 * @param limitToPage the limit to page
	 */
	public void selectItems(boolean select, boolean limitToPage) {
		SortedCellTable<CatalogueDataset> table = getCellTable();
		int rowSize = table.getVisibleItemCount();

		for (int i = 0; i < rowSize; i++) {
			CatalogueDataset item = table.getVisibleItem(i);
			itemsTable.getSelectionModel().setSelected(item, select);
		}
	}

	/**
	 * Gets the select items.
	 *
	 * @return the select items
	 */
	public List<CatalogueDataset> getSelectItems() {
		return itemsTable.getSelectedItems();
	}

	/**
	 * A custom {@link AsyncDataProvider}.
	 *
	 * @author Francesco Mangiacrapa francesco.mangiacrapa@isti.cnr.it Jul 5, 2017
	 * @param <T> the generic type
	 */
	public class MyCustomDataProvider<T> extends AsyncDataProvider<T> {

		/**
		 * {@link #onRangeChanged(HasData)} is called when the table requests a new
		 * range of data. You can push data back to the displays using
		 * {@link #updateRowData(int, List)}.
		 *
		 * @param display the display
		 */
		@Override
		public void onRangeChanged(HasData<T> display) {

			// Get the new range.
			final Range range = display.getVisibleRange();

			int start = range.getStart();
			int length = range.getLength();

			if (initClassFirstRangeChanged) {
				GWT.log("initClassFirstRangeChanged is true.. returning");
				initClassFirstRangeChanged = false;
				return;
			}
			GWT.log("Range changed: " + start + " " + length + " visible count: " + display.getVisibleItemCount());
			loadNewPage(start, length, false);
			eventBus.fireEvent(new TableRangeViewChangedEvent<T>(start, length));

		}

	}

	public void hideSelectableRow(boolean bool) {
		Element tableEl = itemsTable.getCellTable().getElement();
		if (bool)
			tableEl.addClassName("hide_checkbox");
		else
			tableEl.removeClassName("hide_checkbox");

	}

}
