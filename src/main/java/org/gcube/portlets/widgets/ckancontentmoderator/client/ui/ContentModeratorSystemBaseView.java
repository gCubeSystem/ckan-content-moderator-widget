package org.gcube.portlets.widgets.ckancontentmoderator.client.ui;

import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.Widget;

/**
 * The Class ContentModeratorSystemBaseView.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Jun 16, 2021
 */
public class ContentModeratorSystemBaseView {

	private BaseViewTemplate baseViewTemplate = new BaseViewTemplate();
	private ScrollPanel centerPanel = new ScrollPanel();

	/**
	 * Instantiates a new content moderator system base view.
	 */
	public ContentModeratorSystemBaseView() {
		baseViewTemplate.addToMiddle(centerPanel);
	}

	/**
	 * Adds the to top.
	 *
	 * @param w the w
	 */
	public void addToTop(Widget w) {
		baseViewTemplate.addToTop(w);
	}

	/**
	 * Adds the to center.
	 *
	 * @param w the w
	 */
	public void addToCenter(Widget w) {
		centerPanel.add(w);
	}

	/**
	 * Adds the to bottom.
	 *
	 * @param w the w
	 */
	public void addToBottom(Widget w) {
		baseViewTemplate.addToBottom(w);
	}

	/**
	 * Gets the panel.
	 *
	 * @return the panel
	 */
	public Composite getPanel() {
		return baseViewTemplate;
	}
}
