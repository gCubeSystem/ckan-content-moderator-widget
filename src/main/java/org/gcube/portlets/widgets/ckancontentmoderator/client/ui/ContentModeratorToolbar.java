package org.gcube.portlets.widgets.ckancontentmoderator.client.ui;

import java.util.List;

import org.gcube.datacatalogue.utillibrary.shared.ItemStatus;
import org.gcube.portlets.widgets.ckancontentmoderator.client.events.CloseAllTabsEvent;
import org.gcube.portlets.widgets.ckancontentmoderator.client.events.SelectItemsWithItemStatusEvent;
import org.gcube.portlets.widgets.ckancontentmoderator.shared.ItemFieldDV;

import com.github.gwtbootstrap.client.ui.Dropdown;
import com.github.gwtbootstrap.client.ui.Label;
import com.github.gwtbootstrap.client.ui.NavLink;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;

public class ContentModeratorToolbar extends Composite {

	private static ContentModeratorToolbarUiBinder uiBinder = GWT.create(ContentModeratorToolbarUiBinder.class);

	interface ContentModeratorToolbarUiBinder extends UiBinder<Widget, ContentModeratorToolbar> {
	}

	@UiField
	Dropdown dropdownSelectStatus;

	@UiField
	Dropdown dropdownSortBy;

	@UiField
	NavLink closeAllTabs;

	@UiField
	Label statusInfo;
	
	@UiField
	Label orderInfo;

	private HandlerManager eventBus;

	private ItemStatus activeStatus;

	private List<ItemFieldDV> sortByList;

	private ItemFieldDV activeSortBy;

	public ContentModeratorToolbar(HandlerManager eventBus, ItemStatus status, List<ItemFieldDV> sortBy) {
		initWidget(uiBinder.createAndBindUi(this));
		this.eventBus = eventBus;
		this.sortByList = sortBy;
		setActiveStatus(status);
		setActiveSortBy(sortBy.get(0));
		fillSelectionOptions();
		bindEvents();

	}

	private void bindEvents() {
		closeAllTabs.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				eventBus.fireEvent(new CloseAllTabsEvent());

			}
		});
	}

	private void fillSelectionOptions() {
		for (final ItemStatus status : ItemStatus.values()) {

			NavLink navLink = new NavLink();
			navLink.setText(status.getLabel());
			navLink.addClickHandler(new ClickHandler() {

				@Override
				public void onClick(ClickEvent event) {
					GWT.log("clicked: " + status);
					setActiveStatus(status);
					eventBus.fireEvent(new SelectItemsWithItemStatusEvent(status, null, null, getActiveSortBy()));

				}
			});
			dropdownSelectStatus.add(navLink);
		}

		for (final ItemFieldDV itemField : sortByList) {

			NavLink navLink = new NavLink();
			navLink.setText(itemField.getDisplayName());
			navLink.addClickHandler(new ClickHandler() {

				@Override
				public void onClick(ClickEvent event) {
					setActiveSortBy(itemField);
					eventBus.fireEvent(new SelectItemsWithItemStatusEvent(getActiveStatus(), null, null, itemField));

				}

			});
			dropdownSortBy.add(navLink);
		}
	}

	private void setActiveSortBy(ItemFieldDV itemField) {
		this.activeSortBy = itemField;
		this.orderInfo.setText(activeSortBy.getDisplayName());
	}

	private void setActiveStatus(ItemStatus status) {
		this.activeStatus = status;
		this.statusInfo.setText(activeStatus.getLabel());

	}

	public ItemFieldDV getActiveSortBy() {

		if (activeSortBy != null)
			return activeSortBy;

		NavLink navLink = dropdownSortBy.getLastSelectedNavLink();
		String selectedText = navLink.getText();
		for (ItemFieldDV itemField : sortByList) {

			if (itemField.getDisplayName().compareTo(selectedText) == 0) {
				return itemField;
			}
		}

		return sortByList.get(0);
	}

	public ItemStatus getActiveStatus() {
		return activeStatus;
	}

}
