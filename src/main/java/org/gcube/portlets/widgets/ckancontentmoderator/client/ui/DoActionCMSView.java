package org.gcube.portlets.widgets.ckancontentmoderator.client.ui;

import java.util.List;

import org.gcube.datacatalogue.utillibrary.shared.ItemStatus;
import org.gcube.portlets.widgets.ckancontentmoderator.shared.CatalogueDataset;

import com.github.gwtbootstrap.client.ui.AlertBlock;
import com.github.gwtbootstrap.client.ui.Button;
import com.github.gwtbootstrap.client.ui.CheckBox;
import com.github.gwtbootstrap.client.ui.HelpBlock;
import com.github.gwtbootstrap.client.ui.TextArea;
import com.github.gwtbootstrap.client.ui.constants.AlertType;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Widget;

/**
 * The Class DoActionCMSView.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Jun 29, 2021
 */
public class DoActionCMSView extends Composite {

	private static DoActionCMSUiBinder uiBinder = GWT.create(DoActionCMSUiBinder.class);

	/**
	 * The Interface DoActionCMSUiBinder.
	 *
	 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
	 * 
	 *         Jun 29, 2021
	 */
	interface DoActionCMSUiBinder extends UiBinder<Widget, DoActionCMSView> {
	}

	@UiField
	HTML htmlDisplayMessage;

	@UiField
	CheckBox checkBoxPermDelete;

	@UiField
	TextArea txtMsgReason;

	@UiField
	Button buttonActionConfirmYES;

	@UiField
	Button buttonActionConfirmNO;

	@UiField
	AlertBlock alertBlockDoAction;
	
	@UiField
	HelpBlock helpPermDelete;


	private List<CatalogueDataset> listSelectItems;

	private ItemStatus fromStatus;

	private ItemStatus toStatus;

	/**
	 * Instantiates a new do action CMS view.
	 *
	 * @param fromStatus      the from status
	 * @param toStatus        the to status
	 * @param listSelectItems the list select items
	 */
	public DoActionCMSView() {
		initWidget(uiBinder.createAndBindUi(this));
		this.alertBlockDoAction.setClose(true);
		this.alertBlockDoAction.setAnimation(true);
		this.alertBlockDoAction.setType(AlertType.INFO);
		this.checkBoxPermDelete.setVisible(false);
		this.txtMsgReason.setVisible(false);
		
		checkBoxPermDelete.addValueChangeHandler(new ValueChangeHandler<Boolean>() {
			
			@Override
			public void onValueChange(ValueChangeEvent<Boolean> event) {
				helpPermDelete.setVisible(false);
				if(event.getValue()) {
					helpPermDelete.setVisible(true);
				}
			}
		});

	}

	public void updateStatus(ItemStatus fromStatus, ItemStatus toStatus, List<CatalogueDataset> listSelectItems) {
		this.fromStatus = fromStatus;
		this.toStatus = toStatus;
		this.listSelectItems = listSelectItems;
		int count = listSelectItems.size();
		String msg = "Going to update the status of";
		if (count > 0) {
			if (count == 1) {
				msg += " one item";
			} else {
				msg += " " + count + " items";
			}

			msg += " from <b>" + fromStatus.getLabel() + "</b> to <b>" + toStatus.getLabel() + "</b>. Confirm?";
		} else
			return;

		switch (toStatus) {
		case PENDING:
			checkBoxPermDelete.setVisible(false);
			txtMsgReason.setVisible(true);
			txtMsgReason.setPlaceholder("(Optional) Type a message...");
			break;
		case APPROVED:
			checkBoxPermDelete.setVisible(false);
			txtMsgReason.setVisible(true);
			txtMsgReason.setPlaceholder("(Optional) Type a message...");
			break;
		case REJECTED:
			checkBoxPermDelete.setVisible(true);
			txtMsgReason.setVisible(true);
			txtMsgReason.setPlaceholder("(Optional) Rejecting reason...");
			break;
		default:
			break;
		}

		htmlDisplayMessage.setHTML(msg);
	}

	public void permanentlyDelete(List<CatalogueDataset> listSelectItems) {
		this.listSelectItems = listSelectItems;
		int count = listSelectItems.size();
		String msg = "Going to delete permanently ";
		if (count > 0) {
			if (count == 1) {
				msg += " one item";
			} else {
				msg += " " + count + " items";
			}

			msg += " from Catalogue. This operation cannot be undone. Would you like to proceed?";
		} else
			return;

		htmlDisplayMessage.setHTML(msg);
	}

	/**
	 * Checks if is permanently delete.
	 *
	 * @return true, if is permanently delete
	 */
	boolean isPermanentlyDelete() {
		return checkBoxPermDelete.getValue();
	}

	/**
	 * Gets the txt reason msg.
	 *
	 * @return the txt reason msg
	 */
	public String getTxtReasonMsg() {
		return txtMsgReason.getValue();
	}

	/**
	 * Gets the list select items.
	 *
	 * @return the list select items
	 */
	public List<CatalogueDataset> getListSelectItems() {
		return listSelectItems;
	}

	/**
	 * Gets the from status.
	 *
	 * @return the from status
	 */
	public ItemStatus getFromStatus() {
		return fromStatus;
	}

	/**
	 * Gets the to status.
	 *
	 * @return the to status
	 */
	public ItemStatus getToStatus() {
		return toStatus;
	}

	/**
	 * Gets the button action confirm YES.
	 *
	 * @return the button action confirm YES
	 */
	public Button getButtonActionConfirmYES() {
		return buttonActionConfirmYES;
	}

	/**
	 * Gets the button action confirm NO.
	 *
	 * @return the button action confirm NO
	 */
	public Button getButtonActionConfirmNO() {
		return buttonActionConfirmNO;
	}

	/**
	 * Gets the alert block do action.
	 *
	 * @return the alert block do action
	 */
	public AlertBlock getAlertBlockDoAction() {
		return alertBlockDoAction;
	}

}
