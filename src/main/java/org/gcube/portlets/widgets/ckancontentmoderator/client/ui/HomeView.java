package org.gcube.portlets.widgets.ckancontentmoderator.client.ui;

import java.util.ArrayList;
import java.util.List;

import org.gcube.datacatalogue.utillibrary.shared.ItemStatus;
import org.gcube.portlets.widgets.ckancontentmoderator.client.CkanContentModeratorWidgetController;
import org.gcube.portlets.widgets.ckancontentmoderator.client.events.CheckBoxSelectIemsEvent;
import org.gcube.portlets.widgets.ckancontentmoderator.client.events.SelectItemsWithItemStatusEvent;
import org.gcube.portlets.widgets.ckancontentmoderator.client.ui.util.LoadingPanel;
import org.gcube.portlets.widgets.ckancontentmoderator.client.ui.util.UtilFunct;
import org.gcube.portlets.widgets.ckancontentmoderator.shared.CatalogueDataset;
import org.gcube.portlets.widgets.ckancontentmoderator.shared.DISPLAY_FIELD;
import org.gcube.portlets.widgets.ckancontentmoderator.shared.OperationReport;

import com.github.gwtbootstrap.client.ui.AlertBlock;
import com.github.gwtbootstrap.client.ui.Button;
import com.github.gwtbootstrap.client.ui.CheckBox;
import com.github.gwtbootstrap.client.ui.Dropdown;
import com.github.gwtbootstrap.client.ui.Heading;
import com.github.gwtbootstrap.client.ui.NavLink;
import com.github.gwtbootstrap.client.ui.base.AlertBase;
import com.github.gwtbootstrap.client.ui.constants.AlertType;
import com.github.gwtbootstrap.client.ui.event.ClosedEvent;
import com.github.gwtbootstrap.client.ui.event.ClosedHandler;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.Widget;

/**
 * The Class HomeView.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Jun 22, 2021
 */
public class HomeView extends Composite {

	private static HomeViewUiBinder uiBinder = GWT.create(HomeViewUiBinder.class);
	private ContentModeratorSystemBaseView cmsPanel = new ContentModeratorSystemBaseView();
	private ContentModeratorPaginatedView paginatedView;

	@UiField
	HTMLPanel panelContainer;

	@UiField
	CheckBox cbSelectAll;

	@UiField
	Dropdown dropdownSetStatus;

	@UiField
	Heading pageHeader;

	@UiField
	HTMLPanel actionLoaderPanel;

	@UiField
	HTMLPanel confirmPanel;

	@UiField
	Button permanentlyDelete;

	@UiField
	HorizontalPanel updateStatusActionPanel;

	private ScrollPanel confirmPanelContainer = new ScrollPanel();

	private List<NavLink> setStatusOptions = new ArrayList<NavLink>();

	private ItemStatus displayingItemStatus;

	private HandlerManager eventBus;

	private LoadingPanel actionInProgressLoader = new LoadingPanel(new HTML("Performing action..."));
	private boolean readOnlyMode;
	private boolean restrictDataToLoggedInUser;

	/**
	 * The Interface HomeViewUiBinder.
	 *
	 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
	 * 
	 *         Jun 22, 2021
	 */
	interface HomeViewUiBinder extends UiBinder<Widget, HomeView> {
	}

	/**
	 * Instantiates a new home view.
	 *
	 * @param eventBus                   the event bus
	 * @param status                     the status
	 * @param displayFields              the display fields
	 * @param sortByField                the sort by field
	 * @param readOnlyMode               the read only mode
	 * @param restrictDataToLoggedInUser the restrict data to logged in user
	 */
	public HomeView(HandlerManager eventBus, ItemStatus status, DISPLAY_FIELD[] displayFields,
			DISPLAY_FIELD sortByField, boolean readOnlyMode, boolean restrictDataToLoggedInUser) {
		initWidget(uiBinder.createAndBindUi(this));
		setDisplayingWithStatus(status);
		this.eventBus = eventBus;
		this.readOnlyMode = readOnlyMode;
		this.restrictDataToLoggedInUser = restrictDataToLoggedInUser;
		paginatedView = new ContentModeratorPaginatedView(eventBus, status, displayFields, sortByField,
				restrictDataToLoggedInUser);
		cmsPanel.addToCenter(paginatedView.getCellPanel());
		cmsPanel.addToBottom(paginatedView.getPagerPanel());
		panelContainer.add(cmsPanel.getPanel());
		setVisibleUpdateStatusAction(false);
		setVisiblePermanentlyDelete(false);

		// Filling the Dropdown with status options
		for (ItemStatus iStatus : ItemStatus.values()) {
			NavLink nl = new NavLink(iStatus.getLabel());
			dropdownSetStatus.add(nl);
			setStatusOptions.add(nl);

		}

		showActionLoader(false);
		actionLoaderPanel.add(actionInProgressLoader);

		setStatusOptions(status);
		bindEvents();
		confirmPanel.add(confirmPanelContainer);

	}

	/**
	 * Show action loader.
	 *
	 * @param showActionInProgress the show action in progress
	 */
	private void showActionLoader(boolean showActionInProgress) {
		GWT.log("Show action in progress: " + showActionInProgress);
		actionLoaderPanel.setVisible(showActionInProgress);
	}

	/**
	 * Sets the status options according to item status selected.
	 *
	 * @param selectedStatus the new status options
	 */
	private void setStatusOptions(ItemStatus selectedStatus) {

		// Fired when user clicks on CMS action
		for (final NavLink navLink : setStatusOptions) {
			navLink.setVisible(true);
			// Hiding the status selected/displayed from navigation link
			if (navLink.getText().trim().equals(displayingItemStatus.getLabel())) {
				navLink.setVisible(false);
			}

			// Hiding the changing REJECT -> APPROVED
			// One item REJECT must be only updated to return to PENDING status
			if (selectedStatus.equals(ItemStatus.REJECTED)) {
				if (navLink.getText().trim().equals(ItemStatus.APPROVED.getLabel())) {
					navLink.setVisible(false);
				}
			}

			// Hiding the changing of any status from the APPROVED
			if (selectedStatus.equals(ItemStatus.APPROVED)) {
				navLink.setVisible(false);
			}

		}
	}

	/**
	 * Sets the displaying with status.
	 *
	 * @param status the new displaying with status
	 */
	private void setDisplayingWithStatus(ItemStatus status) {
		this.displayingItemStatus = status;
		this.pageHeader.setSubtext(status.getLabel());
	}

	/**
	 * Bind events.
	 */
	public void bindEvents() {

		cbSelectAll.addValueChangeHandler(new ValueChangeHandler<Boolean>() {

			@Override
			public void onValueChange(ValueChangeEvent<Boolean> event) {
				GWT.log("Is checked: " + cbSelectAll.getValue());
				eventBus.fireEvent(new CheckBoxSelectIemsEvent<CatalogueDataset>(cbSelectAll.getValue()));

			}
		});

		// Fired when user clicks on CMS action
		for (final NavLink navLink : setStatusOptions) {
			navLink.addClickHandler(new ClickHandler() {

				@Override
				public void onClick(ClickEvent event) {
					if (paginatedView.getSelectItems().size() > 0) {
						showDoActionView(navLink);
					}

				}
			});
		}

		permanentlyDelete.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				if (paginatedView.getSelectItems().size() > 0) {
					showDoActionViewDeletePermanently();

				}

			}
		});
	}

	/**
	 * Gets the panel.
	 *
	 * @return the panel
	 */
	public Composite getPanel() {
		return this;
	}

	/**
	 * Sets the visible update status action.
	 *
	 * @param bool the new visible update status action
	 */
	public void setVisibleUpdateStatusAction(boolean bool) {
		dropdownSetStatus.setVisible(bool);
	}

	/**
	 * Sets the visible permanently delete.
	 *
	 * @param bool the new visible permanently delete
	 */
	public void setVisiblePermanentlyDelete(boolean bool) {
		permanentlyDelete.setVisible(bool);
	}

	/**
	 * Mark items as checked.
	 *
	 * @param select      the select
	 * @param limitToPage the limit to page
	 */
	public void markItemsAsChecked(boolean select, boolean limitToPage) {
		paginatedView.selectItems(select, limitToPage);
	}

	/**
	 * Sets the checked checkbox select all.
	 *
	 * @param bool the new checked checkbox select all
	 */
	public void setCheckedCheckboxSelectAll(boolean bool) {
		cbSelectAll.setValue(bool);
	}

	/**
	 * Show do action view.
	 *
	 * @param navLink the nav link
	 */
	private void showDoActionView(NavLink navLink) {
		confirmPanelContainer.clear();
		GWT.log("showDoActionView clicked link: " + navLink.getText());
		ItemStatus fromStatus = displayingItemStatus;
		final ItemStatus toStatus = UtilFunct.toStatusFromStatusLabel(navLink.getText().trim());
		GWT.log("Do action on from: " + fromStatus + " to: " + toStatus);
		if (toStatus == null) {
			return;
		}

		final DoActionCMSView doActionCMS = new DoActionCMSView();
		final List<CatalogueDataset> selectedItems = paginatedView.getSelectItems();
		doActionCMS.updateStatus(fromStatus, toStatus, selectedItems);

		final AlertBlock newAlertBlock = doActionCMS.getAlertBlockDoAction();
		newAlertBlock.addClosedHandler(new ClosedHandler<AlertBase>() {

			@Override
			public void onClosed(ClosedEvent<AlertBase> event) {
				confirmPanelContainer.clear();
			}
		});

		Button buttActionConfirmYes = doActionCMS.getButtonActionConfirmYES();
		buttActionConfirmYes.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				confirmPanelContainer.clear();
				performCMSAction(doActionCMS);
			}
		});

		Button buttonActionConfirmNo = doActionCMS.getButtonActionConfirmNO();
		buttonActionConfirmNo.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				confirmPanelContainer.clear();

			}
		});

		confirmPanelContainer.add(doActionCMS);
	}

	/**
	 * Show do action delete permanently.
	 */
	private void showDoActionViewDeletePermanently() {

		confirmPanelContainer.clear();
		GWT.log("showDoActionViewDeletePermanently...");

		final DoActionCMSView doActionCMS = new DoActionCMSView();
		final List<CatalogueDataset> selectedItems = paginatedView.getSelectItems();
		doActionCMS.permanentlyDelete(selectedItems);

		final AlertBlock newAlertBlock = doActionCMS.getAlertBlockDoAction();
		newAlertBlock.addClosedHandler(new ClosedHandler<AlertBase>() {

			@Override
			public void onClosed(ClosedEvent<AlertBase> event) {
				confirmPanelContainer.clear();
			}
		});

		Button buttActionConfirmYes = doActionCMS.getButtonActionConfirmYES();
		buttActionConfirmYes.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				confirmPanelContainer.clear();
				showActionLoader(true);

				List<String> listDatasetNames = UtilFunct.toListDatasetNames(doActionCMS.getListSelectItems());
				CkanContentModeratorWidgetController.contentModeratorService.permanentlyDelete(listDatasetNames,
						new AsyncCallback<OperationReport>() {

							@Override
							public void onFailure(Throwable caught) {
								showActionLoader(false);
								Window.alert(caught.getMessage());
								eventBus.fireEvent(new SelectItemsWithItemStatusEvent(displayingItemStatus,
										"Sorry an error occurred. Please, refresh and try again", AlertType.ERROR));

							}

							@Override
							public void onSuccess(OperationReport result) {
								showActionLoader(false);
								AlertType alert = AlertType.SUCCESS;
								int count = selectedItems.size();
								String msg = "Deleted permanently";
								if (count > 0) {
									if (count == 1) {
										msg += " one item";
									} else {
										msg += " " + count + " items";
									}

									msg += " from Catalogue";
								}

								int errorCount = result.getErrorMapItems().size();
								if (errorCount > 0) {
									msg += " <br/>Error occurred on deleting permanently " + errorCount + " item/s:";
									for (String key : result.getErrorMapItems().keySet()) {
										msg += "<br/> " + key + ". Error: " + result.getErrorMapItems().get(key);
									}

									alert = AlertType.WARNING;
								}

								eventBus.fireEvent(
										new SelectItemsWithItemStatusEvent(displayingItemStatus, msg, alert));

							}
						});
			}
		});

		Button buttonActionConfirmNo = doActionCMS.getButtonActionConfirmNO();
		buttonActionConfirmNo.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				confirmPanelContainer.clear();

			}
		});

		confirmPanelContainer.add(doActionCMS);
	}

	/**
	 * Load form server the items with status.
	 *
	 * @param itemStatus   the item status
	 * @param sortForField
	 */
	public void loadItemsWithStatus(ItemStatus itemStatus, String sortForField) {
		GWT.log("loadItemsWithStatus started");
		setDisplayingWithStatus(itemStatus);
		setCheckedCheckboxSelectAll(false);
		setVisibleUpdateStatusAction(false);
		setVisiblePermanentlyDelete(false);
		setStatusOptions(itemStatus);
		confirmPanelContainer.clear();
		if (itemStatus.equals(ItemStatus.REJECTED))
			setVisiblePermanentlyDelete(true);

		paginatedView.loadItemsForStatus(itemStatus, sortForField);

		GWT.log("loadItemsWithStatus end");
	}

	/**
	 * Perform CMS action.
	 *
	 * @param doActionCMSView the do action CMS view
	 */
	private void performCMSAction(final DoActionCMSView doActionCMSView) {

		showActionLoader(true);
		final ItemStatus toStatus = doActionCMSView.getToStatus();
		List<String> listDatasetNames = UtilFunct.toListDatasetNames(doActionCMSView.getListSelectItems());
		switch (toStatus) {
		case PENDING:

			CkanContentModeratorWidgetController.contentModeratorService.setStatus(toStatus, listDatasetNames,
					doActionCMSView.getTxtReasonMsg(), new AsyncCallback<OperationReport>() {

						@Override
						public void onSuccess(OperationReport result) {
							showActionLoader(false);
							AlertType alert = AlertType.SUCCESS;
							int count = result.getPassedListItems().size();
							String msg = "";
							if (count > 0) {
								if (count == 1) {
									msg += "One item";
								} else {
									msg += count + " items";
								}

								msg += " moved to " + toStatus + " status.";
							}

							int errorCount = result.getErrorMapItems().size();
							if (errorCount > 0) {
								msg += " <br/>Error occurred on updating status to " + errorCount + " item/s:";
								for (String key : result.getErrorMapItems().keySet()) {
									msg += "<br/> " + key + ". Error: " + result.getErrorMapItems().get(key);
								}

								alert = AlertType.WARNING;
							}

							eventBus.fireEvent(new SelectItemsWithItemStatusEvent(displayingItemStatus, msg, alert));
						}

						@Override
						public void onFailure(Throwable caught) {
							showActionLoader(false);
							Window.alert(caught.getMessage());
							eventBus.fireEvent(new SelectItemsWithItemStatusEvent(displayingItemStatus,
									"Sorry an error occurred. Please, refresh and try again", AlertType.ERROR));

						}
					});

			break;

		case APPROVED:

			CkanContentModeratorWidgetController.contentModeratorService.approveItem(listDatasetNames,
					doActionCMSView.getTxtReasonMsg(), new AsyncCallback<OperationReport>() {

						@Override
						public void onFailure(Throwable caught) {
							showActionLoader(false);
							Window.alert(caught.getMessage());
							eventBus.fireEvent(new SelectItemsWithItemStatusEvent(displayingItemStatus,
									"Sorry an error occurred. Please, refresh and try again", AlertType.ERROR));

						}

						@Override
						public void onSuccess(OperationReport result) {
							showActionLoader(false);
							AlertType alert = AlertType.SUCCESS;
							int count = result.getPassedListItems().size();
							String msg = "";
							if (count > 0) {
								if (count == 1) {
									msg += "One item";
								} else {
									msg += count + " items";
								}

								msg += " moved to " + toStatus + " status.";
							}

							int errorCount = result.getErrorMapItems().size();
							if (errorCount > 0) {
								msg += " <br/>Error occurred on approving " + errorCount + " item/s:";
								for (String key : result.getErrorMapItems().keySet()) {
									msg += "<br/> " + key + ". Error: " + result.getErrorMapItems().get(key);
								}

								alert = AlertType.WARNING;
							}

							eventBus.fireEvent(new SelectItemsWithItemStatusEvent(displayingItemStatus, msg, alert));

						}
					});

			break;

		case REJECTED:

			CkanContentModeratorWidgetController.contentModeratorService.rejectItem(listDatasetNames,
					doActionCMSView.isPermanentlyDelete(), doActionCMSView.getTxtReasonMsg(),
					new AsyncCallback<OperationReport>() {

						@Override
						public void onFailure(Throwable caught) {
							showActionLoader(false);
							Window.alert(caught.getMessage());
							eventBus.fireEvent(new SelectItemsWithItemStatusEvent(displayingItemStatus,
									"Sorry an error occurred. Please, refresh and try again", AlertType.ERROR));
						}

						@Override
						public void onSuccess(OperationReport result) {
							showActionLoader(false);
							AlertType alert = AlertType.SUCCESS;
							int count = result.getPassedListItems().size();
							String msg = "";
							if (count > 0) {
								if (count == 1) {
									msg += "One item";
								} else {
									msg += count + " items";
								}

								boolean isPermanentlyDelete = doActionCMSView.isPermanentlyDelete();

								if (isPermanentlyDelete) {
									msg += " deleted permanently from Catalogue";
								} else {
									msg += " moved to " + toStatus + " status.";
								}
							}

							int errorCount = result.getErrorMapItems().size();
							if (errorCount > 0) {
								msg += " <br/>Error occurred on rejecting " + errorCount + " item/s:";
								for (String key : result.getErrorMapItems().keySet()) {
									msg += "<br/> " + key + ". Error: " + result.getErrorMapItems().get(key);
								}

								alert = AlertType.WARNING;
							}

							eventBus.fireEvent(new SelectItemsWithItemStatusEvent(displayingItemStatus, msg, alert));

						}
					});

			break;

		default:
			break;
		}
	}

	/**
	 * Gets the displaying item status.
	 *
	 * @return the displaying item status
	 */
	public ItemStatus getDisplayingItemStatus() {
		return displayingItemStatus;
	}

	/**
	 * Hide update status action.
	 *
	 * @param readOnlyMode the read only mode
	 */
	public void hideUpdateStatusAction(boolean readOnlyMode) {
		updateStatusActionPanel.setVisible(!readOnlyMode);
	}

	/**
	 * Hide selectable row.
	 *
	 * @param bool the bool
	 */
	public void hideSelectableRow(boolean bool) {
		paginatedView.hideSelectableRow(bool);
	}

	/**
	 * Checks if is read only mode.
	 *
	 * @return true, if is read only mode
	 */
	public boolean isReadOnlyMode() {
		return readOnlyMode;
	}

	/**
	 * Checks if is restrict data to logged in user.
	 *
	 * @return true, if is restrict data to logged in user
	 */
	public boolean isRestrictDataToLoggedInUser() {
		return restrictDataToLoggedInUser;
	}

}
