package org.gcube.portlets.widgets.ckancontentmoderator.client.ui;

import com.github.gwtbootstrap.client.ui.Label;
import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.Widget;

public class MainPanel extends Composite {

	private static MainPanelUiBinder uiBinder = GWT.create(MainPanelUiBinder.class);

	interface MainPanelUiBinder extends UiBinder<Widget, MainPanel> {
	}
	
	@UiField
	HTMLPanel mainPanel;
	
	@UiField
	HTMLPanel mainPanelContainer;
	
	@UiField
	Label labelLoggedIn;

	public MainPanel() {
		initWidget(uiBinder.createAndBindUi(this));
	}
	
	public void setLoggedLabelText(String text){
		labelLoggedIn.setText(text);
	}
	
	public HTMLPanel getMainPanelContainer() {
		return mainPanelContainer;
	}


}
