package org.gcube.portlets.widgets.ckancontentmoderator.client.ui;

import java.util.ArrayList;
import java.util.List;

import org.gcube.portlets.widgets.ckancontentmoderator.client.ui.util.UtilFunct;

import com.github.gwtbootstrap.client.ui.Icon;
import com.github.gwtbootstrap.client.ui.Tab;
import com.github.gwtbootstrap.client.ui.TabPanel;
import com.github.gwtbootstrap.client.ui.constants.IconType;
import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.Scheduler;
import com.google.gwt.core.client.Scheduler.ScheduledCommand;
import com.google.gwt.dom.client.Style.Cursor;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.LoadEvent;
import com.google.gwt.event.dom.client.LoadHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.Element;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;

// TODO: Auto-generated Javadoc
/**
 * The Class MainTabPanel.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Jun 18, 2021
 */
public class MainTabPanel extends Composite {

	private static MainTabPanelUiBinder uiBinder = GWT.create(MainTabPanelUiBinder.class);

	/**
	 * The Interface MainTabPanelUiBinder.
	 *
	 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
	 * 
	 *         Jun 18, 2021
	 */
	interface MainTabPanelUiBinder extends UiBinder<Widget, MainTabPanel> {
	}

	private List<Tab> results = new ArrayList<Tab>();

	@UiField
	TabPanel mainTabPanel;

	@UiField
	Tab homeTab;

	/**
	 * Instantiates a new main tab panel.
	 */
	public MainTabPanel() {
		initWidget(uiBinder.createAndBindUi(this));
		results.add(homeTab);
	}

	/**
	 * Adds the home widget.
	 *
	 * @param w the w
	 */
	public void addHomeWidget(Widget w) {
		homeTab.add(w);
	}

	/**
	 * Adds the tab.
	 *
	 * @param heading the heading
	 * @param w       the w
	 * @return the tab
	 */
	public Tab addTab(String heading, Widget w, boolean selectTab) {
		final Tab tab = new Tab();

		if (w instanceof CkanShowItemFrame) {
			tab.setCustomIconStyle("icon-rotate-right icon-spin");
			CkanShowItemFrame ckanFramePanel = (CkanShowItemFrame) w;
			ckanFramePanel.getFrame().addLoadHandler(new LoadHandler() {

				@Override
				public void onLoad(LoadEvent event) {
					tab.setIcon(IconType.BOOK);
					setNoSpinner(tab);
				}
			});
		}

		tab.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
			}
		});

		Icon icon = new Icon(IconType.REMOVE);
		icon.setTitle("Close this tab");
		icon.getElement().getStyle().setCursor(Cursor.POINTER);
		ClickHandler clickHandler = new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				try {
					mainTabPanel.remove(tab);
					results.remove(tab);
					mainTabPanel.selectTab(results.size() - 1);
				} catch (Exception e) {
					// silent
				}
			}
		};

		icon.addDomHandler(clickHandler, ClickEvent.getType());
		tab.addDecorate(icon);

		String shortTitle = UtilFunct.ellipsis(heading, 20, false);
		tab.asWidget().setTitle(heading);
		tab.setHeading(shortTitle);
		if (w != null)
			tab.add(w);

		results.add(tab);
		mainTabPanel.add(tab);
		// activeTabPanels(false);

		if (selectTab) {
			Scheduler.get().scheduleDeferred(new ScheduledCommand() {

				@Override
				public void execute() {
					mainTabPanel.selectTab(results.size() - 1);
				}
			});
		}

		return tab;
	}

	/**
	 * Active tab panels.
	 *
	 * @param bool the bool
	 */
	private void activeTabPanels(boolean bool) {
		for (Tab tab : results) {
			tab.setActive(false);
		}
	}

	/**
	 * Count tab.
	 *
	 * @return the int
	 */
	public int countTab() {

		return results.size();
	}

	/**
	 * Close tabs.
	 */
	public void closeTabs() {
		int tabSize = results.size();
		GWT.log("tab size is: " + tabSize);
		for (int i = 1; i < tabSize; i++) {
			// each remove shifts any subsequent elements to the left, so I'm removing
			// always the first element
			mainTabPanel.remove(1);
			results.remove(1);
		}
		// selecting Home Tab
		selectTab(0);
	}

	public void selectTab(int tabIndex) {
		if (tabIndex <= results.size()) {
			mainTabPanel.selectTab(tabIndex);
		}

	}

	/**
	 * Sets the no spinner.
	 *
	 * @param tab the new no spinner
	 */
	private void setNoSpinner(Tab tab) {
		try {
			tab.asTabLink().getAnchor().removeStyleName("icon-spin");
			tab.asTabLink().getAnchor().removeStyleName("icon-rotate-right");
			Element anchorElem = tab.asTabLink().getAnchor().asWidget().getElement();
			anchorElem.getFirstChildElement().removeClassName("icon-spin");
			anchorElem.getFirstChildElement().removeClassName("icon-rotate-right");
		} catch (Exception e) {
			// silent
		}
		tab.asTabLink().getAnchor().setIcon(IconType.BOOK);
		// tab.asTabLink().getAnchor().setVisible(false);
		// if(tab.asTabLink().getAnchor().setVisible(false);)

	}

}
