/**
 *
 */
package org.gcube.portlets.widgets.ckancontentmoderator.client.ui;

import java.util.List;

/**
 * The Interface SelectionItems.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 * Jun 18, 2021
 */
public interface SelectionItems {
	
	/**
	 * Gets the selected items.
	 *
	 * @param <T> the generic type
	 * @return the selected items
	 */
	<T> List<T> getSelectedItems();

}