/**
 *
 */
package org.gcube.portlets.widgets.ckancontentmoderator.client.ui.table;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Set;

import org.gcube.portlets.widgets.ckancontentmoderator.client.ContentModeratorWidgetConstants;
import org.gcube.portlets.widgets.ckancontentmoderator.client.events.ShowItemEvent;
import org.gcube.portlets.widgets.ckancontentmoderator.client.resources.ContentModeratorWidgetResources;
import org.gcube.portlets.widgets.ckancontentmoderator.client.ui.SelectionItems;
import org.gcube.portlets.widgets.ckancontentmoderator.shared.CatalogueDataset;
import org.gcube.portlets.widgets.ckancontentmoderator.shared.DISPLAY_FIELD;

import com.github.gwtbootstrap.client.ui.Pagination;
import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.cell.client.Cell.Context;
import com.google.gwt.cell.client.CheckboxCell;
import com.google.gwt.cell.client.DateCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.core.shared.GWT;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.resources.client.ImageResource;
import com.google.gwt.safehtml.shared.SafeHtml;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;
import com.google.gwt.safehtml.shared.SafeHtmlUtils;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.ColumnSortEvent;
import com.google.gwt.user.cellview.client.ColumnSortList.ColumnSortInfo;
import com.google.gwt.user.cellview.client.SimplePager;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.view.client.AbstractDataProvider;
import com.google.gwt.view.client.ListDataProvider;
import com.google.gwt.view.client.MultiSelectionModel;
import com.google.gwt.view.client.SelectionModel;
import com.google.gwt.view.client.SingleSelectionModel;

/**
 * The Class ItemsTable.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Jun 15, 2021
 * @param <T> the generic type
 */
public class ItemsTable<T extends CatalogueDataset> extends AbstractItemsCellTable<T> implements SelectionItems {

	private TextColumn<T> type;
	private TextColumn<T> name;
	private TextColumn<T> title;
	private TextColumn<T> author;
	public DateTimeFormat dtformat = DateTimeFormat.getFormat("dd MMM hh:mm aaa yyyy");
	public ImageResource info = ContentModeratorWidgetResources.ICONS.infoSquare();

	private AbstractDataProvider<T> dataProvider;

	private List<DISPLAY_FIELD> displayFields;
	private Column<T, Date> createdColumn;
	private Column<T, Date> lastUpdateColumn;
	private DISPLAY_FIELD startSortByColumn;
	private boolean isAsyncronusTable;

	/**
	 * Instantiates a new items table.
	 *
	 * @param eventBus          the event bus
	 * @param fields            the fields
	 * @param startSortByColumn the start sort by column
	 */
	public ItemsTable(HandlerManager eventBus, DISPLAY_FIELD[] fields, DISPLAY_FIELD startSortByColumn) {
		this.eventBus = eventBus;
		this.startSortByColumn = startSortByColumn;
		setDisplayFields(fields);
	}

	/**
	 * Adds the items.
	 *
	 * @param items the items
	 */
	public void addItems(List<T> items) {
		super.addItems(items);
	}

	/**
	 * Inits the table.
	 *
	 * @param pager        the pager
	 * @param pagination   the pagination
	 * @param dataProvider the data provider
	 */
	@Override
	public void initTable(final SimplePager pager, final Pagination pagination, AbstractDataProvider<T> dataProvider) {
		this.dataProvider = dataProvider;
		this.theSelectionModel = new MultiSelectionModel<T>();
		initAbstractTable(eventBus, fireEventOnClick, dataProvider, theSelectionModel,
				ContentModeratorWidgetConstants.ITEMS_PER_PAGE);
		this.dataProvider.addDataDisplay(sortedCellTable);

		this.isAsyncronusTable = dataProvider instanceof ListDataProvider ? false : true;
		setEmptyTableMessage(ContentModeratorWidgetConstants.NO_DATA);

		final CheckboxCell cellCheckBox = new CheckboxCell(true, false);
		Column<T, Boolean> checkColumn = new Column<T, Boolean>(cellCheckBox) {
			@Override
			public Boolean getValue(T object) {
				// Get the value from the selection model.
				return theSelectionModel.isSelected(object);
			}

			@Override
			public void render(Context context, T object, SafeHtmlBuilder sb) {
				super.render(context, object, sb);
				GWT.log("added checkbox: " + cellCheckBox + " to object: " + object);
			}

		};

		sortedCellTable.addColumn(checkColumn, "", false);
		sortedCellTable.setColumnWidth(checkColumn, 40, Unit.PX);

		ButtonCell previewButton = new ButtonCell();
		Column<T, String> showdItemColumn = new Column<T, String>(previewButton) {
			public String getValue(T object) {
				return "Show";
			}
		};

		showdItemColumn.setFieldUpdater(new FieldUpdater<T, String>() {
			@Override
			public void update(int index, T object, String value) {
				GWT.log("clicked show");
				eventBus.fireEvent(new ShowItemEvent<T>(Arrays.asList(object), false));
			}
		});
		sortedCellTable.addColumn(showdItemColumn);
		sortedCellTable.setColumnWidth(showdItemColumn, 80, Unit.PX);

		if (this.displayFields.contains(DISPLAY_FIELD.NAME)) {

			// NAME
			name = new TextColumn<T>() {
				@Override
				public String getValue(T object) {
					if (object == null)
						return "";
					return ((CatalogueDataset) object).getName();
				}

				// ADDING TOOLTIP
				@Override
				public void render(com.google.gwt.cell.client.Cell.Context context, T object, SafeHtmlBuilder sb) {
					if (object == null)
						return;
					sb.appendHtmlConstant("<div title=\"" + ((CatalogueDataset) object).getName() + "\">");
					super.render(context, object, sb);
					sb.appendHtmlConstant("</div>");
				};
			};

			sortedCellTable.addColumn(name, "Name", true);

			if (!isAsyncronusTable) {
				Comparator<T> c = new Comparator<T>() {
					@Override
					public int compare(T o1, T o2) {
						return ((CatalogueDataset) o1).getName().compareTo(((CatalogueDataset) o2).getName());
					}
				};

				sortedCellTable.setComparator(name, c);
			}

		}

		if (this.displayFields.contains(DISPLAY_FIELD.TITLE)) {

			title = new TextColumn<T>() {
				@Override
				public String getValue(T object) {
					if (object == null)
						return "";
					return ((CatalogueDataset) object).getTitle() != null ? ((CatalogueDataset) object).getTitle() : "";
				}
			};

			sortedCellTable.addColumn(title, "Title", true);

			if (!isAsyncronusTable) {
				Comparator<T> c = new Comparator<T>() {
					@Override
					public int compare(T o1, T o2) {
						return ((CatalogueDataset) o1).getTitle().compareTo(((CatalogueDataset) o2).getTitle());
					}
				};
				sortedCellTable.setComparator(title, c);
			}

		}

		if (this.displayFields.contains(DISPLAY_FIELD.AUTHOR)) {

			author = new TextColumn<T>() {
				@Override
				public String getValue(T object) {
					if (object == null)
						return "";
					return ((CatalogueDataset) object).getAuthor() != null ? ((CatalogueDataset) object).getAuthor()
							: "";
				}
			};

			sortedCellTable.addColumn(author, "Author", true);

			if (!isAsyncronusTable) {
				Comparator<T> c = new Comparator<T>() {
					@Override
					public int compare(T o1, T o2) {
						return ((CatalogueDataset) o1).getAuthor().compareTo(((CatalogueDataset) o2).getAuthor());
					}
				};
				sortedCellTable.setComparator(author, c);
			}

		}

		if (this.displayFields.contains(DISPLAY_FIELD.CREATED)) {

			DateCell date = new DateCell(dtformat);
			createdColumn = new Column<T, Date>(date) {

				@Override
				public Date getValue(T object) {
					if (object == null)
						return null;

					return new Date(((CatalogueDataset) object).getCreated());
				}
			};
			sortedCellTable.addColumn(createdColumn, "Created", true);

			if (!isAsyncronusTable) {
				Comparator<T> c = new Comparator<T>() {
					@Override
					public int compare(T o1, T o2) {
						if (o1 == null || o1.getCreated() == 0)
							return -1;

						if (o2 == null || o2.getCreated() == 0)
							return 1;

						Date d1 = new Date(((CatalogueDataset) o1).getCreated());
						Date d2 = new Date(((CatalogueDataset) o2).getCreated());

						// GWT.log(d1.toString() + "is after "+d2.toString() +" ? "+d2.after(d1));

						if (d1.after(d2))
							return 1;
						else
							return -1;
					}
				};
				GWT.log("date colum sortable");
				sortedCellTable.setComparator(createdColumn, c);
			}

		}

		if (this.displayFields.contains(DISPLAY_FIELD.LAST_UPDATE)) {

			DateCell date = new DateCell(dtformat);
			lastUpdateColumn = new Column<T, Date>(date) {

				@Override
				public Date getValue(T object) {
					if (object == null)
						return null;

					return new Date(((CatalogueDataset) object).getLastUpdate());
				}
			};
			sortedCellTable.addColumn(lastUpdateColumn, "Last Update", true);

			if (!isAsyncronusTable) {
				Comparator<T> c = new Comparator<T>() {
					@Override
					public int compare(T o1, T o2) {
						if (o1 == null || o1.getCreated() == 0)
							return -1;

						if (o2 == null || o2.getCreated() == 0)
							return 1;

						Date d1 = new Date(((CatalogueDataset) o1).getLastUpdate());
						Date d2 = new Date(((CatalogueDataset) o2).getLastUpdate());

						// GWT.log(d1.toString() + "is after "+d2.toString() +" ? "+d2.after(d1));

						if (d1.after(d2))
							return 1;
						else
							return -1;
					}
				};
				GWT.log("date colum sortable");
				sortedCellTable.setComparator(lastUpdateColumn, c);
			}

		}

		if (this.displayFields.contains(DISPLAY_FIELD.TYPE)) {

			// NAME
			type = new TextColumn<T>() {
				@Override
				public String getValue(T object) {
					if (object == null)
						return "";

					String type = ((CatalogueDataset) object).getExtra_SystemType();

					if (type == null)
						return "";
					return type;
				}

				// ADDING TOOLTIP
				@Override
				public void render(com.google.gwt.cell.client.Cell.Context context, T object, SafeHtmlBuilder sb) {
					if (object == null)
						return;
					String type = ((CatalogueDataset) object).getExtra_SystemType();
					if (type == null)
						type = "";
					sb.appendHtmlConstant("<div title=\"" + type + "\">");
					super.render(context, object, sb);
					sb.appendHtmlConstant("</div>");
				};
			};

			sortedCellTable.addColumn(type, "Type", true);

			if (!isAsyncronusTable) {
				Comparator<T> c = new Comparator<T>() {
					@Override
					public int compare(T o1, T o2) {
						if (o1 == null || o1.getExtra_SystemType() == null)
							return -1;

						if (o2 == null || o2.getExtra_SystemType() == null)
							return 1;
						return ((CatalogueDataset) o1).getExtra_SystemType()
								.compareTo(((CatalogueDataset) o2).getExtra_SystemType());
					}
				};

				sortedCellTable.setComparator(type, c);
			}

		}

		GWT.log("startSortByColumn: " + startSortByColumn);

		if (startSortByColumn != null)
			switch (startSortByColumn) {
			case NAME:
				if (this.displayFields.contains(DISPLAY_FIELD.NAME)) {
					sortedCellTable.setInitialSortColumn(name);
				}
				break;
			case TITLE:
				if (this.displayFields.contains(DISPLAY_FIELD.TITLE)) {
					sortedCellTable.setInitialSortColumn(title);
				}
				break;
			case AUTHOR:
				if (this.displayFields.contains(DISPLAY_FIELD.AUTHOR)) {
					sortedCellTable.setInitialSortColumn(author);
				}
				break;
			case TYPE:
				if (this.displayFields.contains(DISPLAY_FIELD.TYPE)) {
					sortedCellTable.setInitialSortColumn(type);
				}
				break;
			case CREATED:
				if (this.displayFields.contains(DISPLAY_FIELD.CREATED)) {
					sortedCellTable.setDefaultSortOrder(createdColumn, false); // sorts ascending on first click
					sortedCellTable.setInitialSortColumn(createdColumn);
					GWT.log("sortedCellTable: " + sortedCellTable);
				}
				break;
			case LAST_UPDATE:
				if (this.displayFields.contains(DISPLAY_FIELD.LAST_UPDATE)) {
					sortedCellTable.setDefaultSortOrder(lastUpdateColumn, false); // sorts ascending on first click
					sortedCellTable.setInitialSortColumn(lastUpdateColumn);
					GWT.log("sortedCellTable: " + sortedCellTable);
				}
				break;
			default:
				break;
			}

	}

	/**
	 * Displays the appropriate sorted icon in the header of the column for the
	 * given index.
	 *
	 * @param columnIndex of the column to mark as sorted
	 * @param ascending   <code>true</code> for ascending icon, <code>false</code>
	 *                    for descending icon
	 */
	public void setSortedColumn(int columnIndex, boolean ascending) {
		GWT.log("Column index: " + columnIndex);
		GWT.log("ascending: " + ascending);
		Column<T, ?> column = sortedCellTable.getColumn(columnIndex);
		if (column != null && column.isSortable()) {
			ColumnSortInfo info = sortedCellTable.getColumnSortList().push(column);
//	           ColumnSortEvent.fire(cellTable, cellTable.getColumnSortList());
			GWT.log("info.isAscending(): " + info.isAscending());
			if (info.isAscending() != ascending) {
				sortedCellTable.getColumnSortList().push(column);
				ColumnSortEvent.fire(sortedCellTable, sortedCellTable.getColumnSortList());
			}
		}
	}

	/**
	 * Sets the display fields.
	 *
	 * @param fields the new display fields
	 */
	public void setDisplayFields(DISPLAY_FIELD[] fields) {
		this.displayFields = fields != null && fields.length > 0 ? Arrays.asList(fields)
				: Arrays.asList(DISPLAY_FIELD.values());
	}

	/**
	 * Reset columns table.
	 */
	public void reInitColumnsTable() {
		int count = sortedCellTable.getColumnCount();
		for (int i = 0; i < count; i++) {
			sortedCellTable.removeColumn(0);
		}
		initTable(null, null, dataProvider);
	}

	/**
	 * Gets the display fields.
	 *
	 * @return the displayFields
	 */
	public List<DISPLAY_FIELD> getDisplayFields() {
		return displayFields;
	}

	/**
	 * The Class ButtonImageCell.
	 *
	 * @author Francesco Mangiacrapa francesco.mangiacrapa@isti.cnr.it Feb 1, 2016
	 */
	public class ButtonImageCell extends ButtonCell {

		/**
		 * Render.
		 *
		 * @param context the context
		 * @param value   the value
		 * @param sb      the sb
		 */
		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * com.google.gwt.cell.client.AbstractSafeHtmlCell#render(com.google.gwt.cell.
		 * client.Cell.Context, java.lang.Object,
		 * com.google.gwt.safehtml.shared.SafeHtmlBuilder)
		 */
		@Override
		public void render(com.google.gwt.cell.client.Cell.Context context, String value, SafeHtmlBuilder sb) {
			SafeHtml html = SafeHtmlUtils.fromTrustedString(new Image(value).toString());
			sb.append(html);
		}
	}

	/**
	 * Gets the selected item.
	 *
	 * @return the selected item
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see org.gcube.portlets.widgets.wsexplorer.client.notification.
	 * SelectionItemHandler#getSelectionItem()
	 */
	@Override
	public List<T> getSelectedItems() {
		if (theSelectionModel instanceof SingleSelectionModel) {
			T selected = ((SingleSelectionModel<T>) theSelectionModel).getSelectedObject();
			if (selected != null) {
				return Arrays.asList(selected);
			}

		} else if (theSelectionModel instanceof MultiSelectionModel) {
			Set<T> selected = ((MultiSelectionModel<T>) theSelectionModel).getSelectedSet();
			if (selected != null) {
				return new ArrayList<T>(selected);
			}
		}

		return null;
	}

	/**
	 * Sets the empty table message.
	 *
	 * @param msg the new empty table message
	 */
	public void setEmptyTableMessage(String msg) {
		msg = msg != null ? msg : ContentModeratorWidgetConstants.NO_DATA;
		if (sortedCellTable != null)
			sortedCellTable.setEmptyTableWidget(new Label(msg));
	}

	/**
	 * Gets the selection model.
	 *
	 * @return the selection model
	 */
	public SelectionModel<T> getSelectionModel() {
		return theSelectionModel;
	}

}
