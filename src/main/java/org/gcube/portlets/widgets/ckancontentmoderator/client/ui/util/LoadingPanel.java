package org.gcube.portlets.widgets.ckancontentmoderator.client.ui.util;

import org.gcube.portlets.widgets.ckancontentmoderator.client.resources.ContentModeratorWidgetResources;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Image;

/**
 * The Class LoadingPanel.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Jun 16, 2021
 */
public class LoadingPanel extends HorizontalPanel {

	protected final Image LOADING_IMAGE = ContentModeratorWidgetResources.getIconLoading().createImage();

	/**
	 * Instantiates a new loading panel.
	 *
	 * @param html the html
	 */
	public LoadingPanel(HTML html) {
		this.getElement().getStyle().setMargin(5.0, Unit.PX);
//		LOADING_PANEL.setSpacing(3);
		this.add(LOADING_IMAGE);

		if (html == null)
			html = new HTML("Loading...");

		html.getElement().getStyle().setPaddingLeft(5.0, Unit.PX);
		this.add(html);
	}
}
