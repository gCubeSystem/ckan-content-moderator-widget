package org.gcube.portlets.widgets.ckancontentmoderator.client.ui.util;

import java.util.ArrayList;
import java.util.List;

import org.gcube.datacatalogue.utillibrary.shared.ItemStatus;
import org.gcube.portlets.widgets.ckancontentmoderator.shared.CatalogueDataset;


/**
 * The Class UtilFunct.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Jun 22, 2021
 */
public class UtilFunct {

	/**
	 * Ellipsis.
	 *
	 * @param value  the value
	 * @param length the length
	 * @param left   the left
	 * @return the string
	 */
	public static String ellipsis(String value, int length, boolean left) {
		if (value.length() < 3)
			return value;

		if (value.length() > length) {
			if (left)
				return "..." + value.substring(value.length() - length + 3);
			else
				return value.substring(0, length) + "...";
		}
		return value;
	}

	/**
	 * To status from status label.
	 *
	 * @param label the label
	 * @return the item status
	 */
	public static ItemStatus toStatusFromStatusLabel(String label) {

		for (ItemStatus theStatus : ItemStatus.values()) {
			if (theStatus.getLabel().compareTo(label.trim()) == 0) {
				return theStatus;

			}
		}

		return null;
	}

	
	/**
	 * To list dataset names.
	 *
	 * @param itemsSelected the items selected
	 * @return the list
	 */
	public static List<String> toListDatasetNames(List<CatalogueDataset> itemsSelected) {

		List<String> listDatasetNames = new ArrayList<String>(itemsSelected.size());
		for (CatalogueDataset catalogueDataset : itemsSelected) {
			listDatasetNames.add(catalogueDataset.getName());
		}

		return listDatasetNames;
	}

}
