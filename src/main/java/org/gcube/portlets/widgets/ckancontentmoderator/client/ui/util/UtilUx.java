package org.gcube.portlets.widgets.ckancontentmoderator.client.ui.util;

import com.github.gwtbootstrap.client.ui.Alert;
import com.github.gwtbootstrap.client.ui.constants.AlertType;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.ui.Panel;

public class UtilUx {

	/**
	 * Show message.
	 *
	 * @param msg              the msg
	 * @param alertType        the alert type
	 * @param closable the closable
	 * @param addToPanel       the add to panel
	 * @param scheduleTimerMls the schedule timer mls
	 */
	public static void showAlert(String msg, AlertType alertType, boolean closable, final Panel addToPanel, final Integer scheduleTimerMls) {

		final Alert alert = new Alert(msg);
		alertType = alertType != null ? alertType : AlertType.INFO;
		alert.setType(alertType);
		alert.setClose(closable);
		alert.setAnimation(true);
		addToPanel.add(alert);

		if (scheduleTimerMls != null) {

			// Cleans the info panel after 12 sec.
			Timer t = new Timer() {

				@Override
				public void run() {
					addToPanel.clear();
				}
			};

			t.schedule(scheduleTimerMls);
		}
	}

}
