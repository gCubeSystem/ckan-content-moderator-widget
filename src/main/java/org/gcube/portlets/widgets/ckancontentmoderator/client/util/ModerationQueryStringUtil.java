package org.gcube.portlets.widgets.ckancontentmoderator.client.util;

import java.util.Arrays;

import org.gcube.datacatalogue.utillibrary.shared.ItemStatus;
import org.gcube.portlets.widgets.ckancontentmoderator.client.ContentModeratorWidgetConstants;
import org.gcube.portlets.widgets.ckancontentmoderator.shared.DISPLAY_FIELD;

import com.google.gwt.core.client.GWT;

/**
 * The Class ModerationQueryStringUtil.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         May 4, 2022
 */
public class ModerationQueryStringUtil {

	public static final String MODERATION_PARAMETER = "moderation";
	public static final String ITEM_NAME_PARAMETER = "item_name";
	public static final String STATUS_PARAMETER = "status";

	/**
	 * The Enum MODERATION_VALUE.
	 *
	 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
	 * 
	 *         May 4, 2022
	 */
	public enum MODERATION_VALUE {
		show
	}

	/**
	 * Instantiates a new moderation query string util.
	 */
	public ModerationQueryStringUtil() {
	}

	/**
	 * To moderation builder.
	 *
	 * @param decodedQueryString the decoded query string
	 * @return the moderation builder
	 */
	public ModerationBuilder toModerationBuilder(String decodedQueryString) {
		ModerationBuilder mbuilder = null;

		if (decodedQueryString == null || decodedQueryString.isEmpty())
			return null;

		String moderationValue = QueryStringUtil.getValueOfParameterInTheURL(MODERATION_PARAMETER, decodedQueryString);

		MODERATION_VALUE toModerationValue = null;
		// Moderation parameter in the query string has a not empty value
		if (moderationValue != null) {
			try {
				toModerationValue = MODERATION_VALUE.valueOf(moderationValue.toLowerCase());
			} catch (Exception e) {
				GWT.log("Moderation value: " + moderationValue + " is not a value of " + MODERATION_VALUE.values());
			}
		}

		if (toModerationValue != null) {

			mbuilder = new ModerationBuilder();

			String itemNameValue = QueryStringUtil.getValueOfParameterInTheURL(ITEM_NAME_PARAMETER, decodedQueryString);
			if (itemNameValue != null) {
				mbuilder.setItemName(itemNameValue);
			}

			String statusValue = QueryStringUtil.getValueOfParameterInTheURL(STATUS_PARAMETER, decodedQueryString);
			if (statusValue != null) {
				try {
					ItemStatus theStatus = ItemStatus.valueOf(statusValue.toUpperCase());
					mbuilder.setItemStatus(theStatus);
				} catch (Exception e) {
					GWT.log("Error on assigning "+ItemStatus.values() +" from string "+statusValue);
				}

			}
		}

		return mbuilder;
	}

	/**
	 * The Class ModerationBuilder.
	 *
	 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
	 * 
	 *         May 4, 2022
	 */
	public class ModerationBuilder {

		private ItemStatus itemStatus = ItemStatus.PENDING;
		private String itemName = null;
		private DISPLAY_FIELD[] displayFields = DISPLAY_FIELD.values();
		private DISPLAY_FIELD[] sortByFields = ContentModeratorWidgetConstants.DEFAULT_SORT_BY_FIELDS;

		/**
		 * Instantiates a new moderation builder.
		 */
		ModerationBuilder() {

		}

		/**
		 * Gets the item name.
		 *
		 * @return the item name
		 */
		public String getItemName() {
			return itemName;
		}

		/**
		 * Sets the item name.
		 *
		 * @param itemName the new item name
		 */
		public void setItemName(String itemName) {
			this.itemName = itemName;
		}

		/**
		 * Gets the item status.
		 *
		 * @return the item status
		 */
		public ItemStatus getItemStatus() {
			return itemStatus;
		}

		/**
		 * Gets the display fields.
		 *
		 * @return the display fields
		 */
		public DISPLAY_FIELD[] getDisplayFields() {
			return displayFields;
		}

		/**
		 * Gets the sort by fields.
		 *
		 * @return the sort by fields
		 */
		public DISPLAY_FIELD[] getSortByFields() {
			return sortByFields;
		}

		/**
		 * Sets the item status.
		 *
		 * @param itemStatus the new item status
		 */
		public void setItemStatus(ItemStatus itemStatus) {
			this.itemStatus = itemStatus;
		}

		/**
		 * Sets the display fields.
		 *
		 * @param displayFields the new display fields
		 */
		public void setDisplayFields(DISPLAY_FIELD[] displayFields) {
			this.displayFields = displayFields;
		}

		/**
		 * Sets the sort by fields.
		 *
		 * @param sortByFields the new sort by fields
		 */
		public void setSortByFields(DISPLAY_FIELD[] sortByFields) {
			this.sortByFields = sortByFields;
		}

		@Override
		public String toString() {
			StringBuilder builder = new StringBuilder();
			builder.append("ModerationBuilder [itemStatus=");
			builder.append(itemStatus);
			builder.append(", itemName=");
			builder.append(itemName);
			builder.append(", displayFields=");
			builder.append(Arrays.toString(displayFields));
			builder.append(", sortByFields=");
			builder.append(Arrays.toString(sortByFields));
			builder.append("]");
			return builder.toString();
		}

	}

	/**
	 * Base 64 decode.
	 *
	 * @param a the a
	 * @return the string
	 */
	protected static native String base64decode(String a) /*-{
		return window.atob(a);
	}-*/;

}
