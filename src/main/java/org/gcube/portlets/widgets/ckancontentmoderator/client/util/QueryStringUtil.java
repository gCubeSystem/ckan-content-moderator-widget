package org.gcube.portlets.widgets.ckancontentmoderator.client.util;

import com.google.gwt.core.client.GWT;
import com.google.gwt.http.client.URL;

/**
 * The Class QueryStringUtil.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         May 3, 2022
 */
public class QueryStringUtil {

	/**
	 * Gets the value of parameter in the URL.
	 *
	 * @param paramName the param name
	 * @param url       the url
	 * @return the value of parameter in the URL
	 */
	public static String getValueOfParameterInTheURL(String paramName, String url) {
		int index = url.toLowerCase().indexOf(paramName.toLowerCase() + "="); // ADDING CHAR "=" IN TAIL TO BE SURE THAT
																				// IT IS A PARAMETER
		String value = null;
		if (index > -1) {

			int start = index + paramName.length() + 1; // add +1 for char '='
			String sub = url.substring(start, url.length());
			int indexOfSeparator = sub.indexOf("&");
			int end = indexOfSeparator != -1 ? indexOfSeparator : sub.length();
			value = sub.substring(0, end);
		}

		GWT.log("Returning value " + value + " for the parameter: " + paramName);
		return value;
	}

	/**
	 * Base 64 decode.
	 *
	 * @param a the a
	 * @return the string
	 */
	private static native String base64decode(String a) /*-{
		return window.atob(a);
	}-*/;

	/**
	 * Base 64 decode query string.
	 *
	 * @param base64EncodedQueryString the base 64 encoded query string
	 * @return the string
	 */
	public static String base64DecodeQueryString(String base64EncodedQueryString) {

		if (base64EncodedQueryString != null && !base64EncodedQueryString.isEmpty()) {
			String b64DecodedQueryStringParameter = base64decode(base64EncodedQueryString);
			GWT.log("Base64 Decoded string is: " + b64DecodedQueryStringParameter);
			b64DecodedQueryStringParameter = URL.decodeQueryString(b64DecodedQueryStringParameter); // equals should be
																									// encoded too (%3D)
			GWT.log("Decoded query string is: " + b64DecodedQueryStringParameter);

			return b64DecodedQueryStringParameter;
		}

		return base64EncodedQueryString;

	}

}
