package org.gcube.portlets.widgets.ckancontentmoderator.server;

import java.util.Map;
import java.util.function.Function;

import org.gcube.datacatalogue.utillibrary.shared.jackan.model.CkanDataset;
import org.gcube.portlets.widgets.ckancontentmoderator.shared.CatalogueDataset;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CatalogueBeansConverter {

	private static Logger LOG = LoggerFactory.getLogger(CatalogueBeansConverter.class);

	/** The to S sync folder descriptor. */
	public static Function<CkanDataset, CatalogueDataset> toCatalogueDataset = new Function<CkanDataset, CatalogueDataset>() {

		public CatalogueDataset apply(CkanDataset t) {
			CatalogueDataset myDataset = new CatalogueDataset();
			if (t == null) {
				LOG.info("Input " + CkanDataset.class.getSimpleName() + " is null, returning empty "
						+ CkanDataset.class.getSimpleName());
				return myDataset;
			}

			myDataset.setAuthor(t.getAuthor());
			myDataset.setAuthorEmail(t.getAuthorEmail());
			
			//is it needed??
			myDataset.setExtra_ItemURL(null);
			
			System.out.println("Extras: "+t.getExtrasAsHashMap());
			Map<String, String> extrasMap = t.getExtrasAsHashMap();
			if(extrasMap!=null) {
				String type = extrasMap.get("system:type");
				myDataset.setExtra_SystemType(type);
				String itemURL = extrasMap.get("Item URL");
				myDataset.setExtra_ItemURL(itemURL);
			}
			
			myDataset.setId(t.getId());
			myDataset.setLicenseId(t.getLicenseId());
			myDataset.setMaintainer(t.getMaintainer());
			myDataset.setMaintainerEmail(t.getMaintainerEmail());
			myDataset.setName(t.getName());
			myDataset.setNotes(t.getNotes());
			myDataset.setOwnerOrg(t.getOwnerOrg());
			myDataset.setTitle(t.getTitle());
			
			//here is always null
			myDataset.setUrl(t.getUrl());
			myDataset.setCreated(t.getMetadataCreated().getTime());
			myDataset.setLastUpdate(t.getMetadataModified().getTime());
			return myDataset;
		}
	};

}
