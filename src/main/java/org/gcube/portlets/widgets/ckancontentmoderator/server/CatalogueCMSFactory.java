package org.gcube.portlets.widgets.ckancontentmoderator.server;

import java.util.concurrent.ConcurrentHashMap;

import org.gcube.datacatalogue.utillibrary.server.DataCatalogueImpl;
import org.gcube.datacatalogue.utillibrary.server.cms.CatalogueContentModeratorSystem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * A factory for getting CatalogueContentModeratorSystem objects.
 * 
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Jun 14, 2021
 */
public class CatalogueCMSFactory {

	private static final Logger logger = LoggerFactory.getLogger(CatalogueCMSFactory.class);
	private static final long MAX_LIFETIME = 1000 * 60 * 30; // 30 MINUTES
	private static CatalogueCMSFactory instance = new CatalogueCMSFactory();
	private static ConcurrentHashMap<String, CacheBean> cache;

	/**
	 * The Class CacheBean.
	 *
	 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
	 * 
	 *         Jun 16, 2021
	 */
	private class CacheBean {
		DataCatalogueImpl dataCatalogueImpl;
		long ttl;

		/**
		 * Instantiates a new cache bean.
		 *
		 * @param ttl               the ttl
		 * @param dataCatalogueImpl the data catalogue impl
		 */
		public CacheBean(long ttl, DataCatalogueImpl dataCatalogueImpl) {
			this.ttl = ttl;
			this.dataCatalogueImpl = dataCatalogueImpl;
		}
	}

	/**
	 * Private constructor.
	 */
	private CatalogueCMSFactory() {

		logger.debug(CatalogueCMSFactory.class.getSimpleName() + " object build");
		cache = new ConcurrentHashMap<String, CacheBean>();

	}

	/**
	 * Get the factory instance.
	 *
	 * @return the factory
	 */
	public static CatalogueCMSFactory getFactory() {
		logger.debug(CatalogueCMSFactory.class.getSimpleName() + " requested");
		return instance;
	}

	/**
	 * Gets the CMS per scope.
	 *
	 * @param scope the scope
	 * @return the CMS per scope
	 * @throws Exception the exception
	 */
	public CatalogueContentModeratorSystem getCMSPerScope(String scope) throws Exception {

		DataCatalogueImpl dataCatalogueImpl = getCatalogueImplPerScope(scope);
		return dataCatalogueImpl.getCatalogueContentModerator();
	}

	/**
	 * Gets the catalogue impl per scope.
	 *
	 * @param scope the scope
	 * @return the catalogue impl per scope
	 * @throws Exception the exception
	 */
	public DataCatalogueImpl getCatalogueImplPerScope(String scope) throws Exception {

		if (scope == null || scope.isEmpty())
			throw new IllegalArgumentException("Invalid scope given!");

		if (cache.containsKey(scope) && !expired(cache.get(scope))) {
			return cache.get(scope).dataCatalogueImpl;
		} else {
			logger.info("Creating " + CatalogueCMSFactory.class.getSimpleName() + " for scope " + scope);
			DataCatalogueImpl dci = new DataCatalogueImpl(scope);
			cache.put(scope, new CacheBean(System.currentTimeMillis(), dci));
			return dci;
		}
	}

	/**
	 * Check if the ckan information must be retrieved again.
	 *
	 * @param cacheBean the cache bean
	 * @return true, if successful
	 */
	private boolean expired(CacheBean cacheBean) {
		return (cacheBean.ttl + MAX_LIFETIME < System.currentTimeMillis());
	}
}
