package org.gcube.portlets.widgets.ckancontentmoderator.server;

import java.util.ArrayList;
import java.util.List;

import org.gcube.vomanagement.usermanagement.model.Email;
import org.gcube.vomanagement.usermanagement.model.GCubeUser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Class CkanContentModeratorUtil.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Apr 6, 2022
 */
public class CkanContentModeratorUtil {

	private static Logger LOG = LoggerFactory.getLogger(CkanContentModeratorUtil.class);

	public static enum QUERY_OPERATOR {
		AND, OR
	}

	/**
	 * User query for emails.
	 *
	 * @param user the user
	 * @return the string representing the value of the query for filtering for User
	 *         Mails
	 * @throws Exception the exception
	 */
	public static String userSOLRQueryForEmails(GCubeUser user, QUERY_OPERATOR operator) throws Exception {
		LOG.debug("userQueryForEmails called for user {}", user);

		List<Email> listServerEmails = user.getEmailAddresses();
		LOG.debug("List server mails: {}", listServerEmails);
		List<String> emailsAddresses = new ArrayList<String>();

		boolean notFoundMail = listServerEmails == null || listServerEmails.isEmpty();

		if (notFoundMail) {
			LOG.warn("No list mail found in " + GCubeUser.class.getSimpleName() + " getEmailAddresses()");
			String mail = user.getEmail();
			LOG.debug("Read mail: " + mail);
			if (mail != null && !mail.isEmpty()) {
				LOG.info("Email found in " + GCubeUser.class.getSimpleName() + " getEmail()");
				emailsAddresses.add(mail);
			}
		} else {
			for (Email email : listServerEmails) {
				emailsAddresses.add(email.getEmail());
			}
		}

		LOG.info("Email/s found for user {} is/are: {}", user.getUsername(), emailsAddresses);

		if (emailsAddresses.isEmpty()) {
			throw new Exception("No email found for user: " + user.getUsername());
		}

		if (operator == null)
			operator = QUERY_OPERATOR.OR;

		StringBuilder queryMails = new StringBuilder();
		String queryOperator = operator.name();
		// BUILDING EMAILS QUERY
		int numberOfEmails = emailsAddresses.size();

		String theQuery = "";

		// case 1 email address
		if (numberOfEmails == 1) {
			theQuery = "'" + emailsAddresses.get(0) + "'";
		} else {
			// case N > 1 email addresses
			for (int i = 0; i < emailsAddresses.size() - 1; i++) {
				String email = emailsAddresses.get(i);
				if (i == 0) {
					// opening the query and adding first email address
					queryMails.append("('" + email + "'");
				} else {
					// adding the operator and the email address
					queryMails.append(" " + queryOperator + " '" + email + "'");
				}
			}

			theQuery = queryMails.toString();

			// to be sure that endsWith Operator
			if (!theQuery.endsWith(queryOperator)) {
				theQuery += " " + queryOperator + " ";
			}

			// adding last email address and closing the query
			theQuery += "'" + emailsAddresses.get(numberOfEmails - 1) + "')";
		}

		return theQuery;
	}
}
