/**
 *
 */
package org.gcube.portlets.widgets.ckancontentmoderator.server;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.gcube.common.portal.PortalContext;
import org.gcube.portlets.widgets.ckancontentmoderator.shared.CMSUserRole;
import org.gcube.vomanagement.usermanagement.RoleManager;
import org.gcube.vomanagement.usermanagement.exception.GroupRetrievalFault;
import org.gcube.vomanagement.usermanagement.exception.UserRetrievalFault;
import org.gcube.vomanagement.usermanagement.impl.LiferayRoleManager;
import org.gcube.vomanagement.usermanagement.model.GCubeRole;
import org.gcube.vomanagement.usermanagement.model.GCubeUser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.liferay.portal.service.UserLocalServiceUtil;

/**
 * The Class GcubeContextUtil.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Feb 21, 2022
 */
public class GcubeContextUtil {

	protected static Logger LOG = LoggerFactory.getLogger(GcubeContextUtil.class);

	/**
	 * Checks if is within portal.
	 *
	 * @return true if you're running into the portal, false if in development
	 */
	public static boolean isWithinPortal() {
		try {
			UserLocalServiceUtil.getService();
			return true;
		} catch (Exception ex) {
			LOG.trace("Development Mode ON");
			return false;
		}
	}

	/**
	 * Gets the current user.
	 *
	 * @param httpServletRequest the http servlet request
	 * @return the current user
	 */
	public static GCubeUser getCurrentUser(HttpServletRequest httpServletRequest) {
		return PortalContext.getConfiguration().getCurrentUser(httpServletRequest);
	}

	/**
	 * Gets the current scope.
	 *
	 * @param httpServletRequest the http servlet request
	 * @return the current scope
	 */
	public static String getCurrentScope(HttpServletRequest httpServletRequest) {
		return PortalContext.getConfiguration().getCurrentScope(httpServletRequest);
	}

	/**
	 * Gets the current token.
	 *
	 * @param scope    the scope
	 * @param username the username
	 * @return the current token
	 */
	public static String getCurrentToken(String scope, String username) {
		return PortalContext.getConfiguration().getCurrentUserToken(scope, username);
	}

	/**
	 * Gets the current group id.
	 *
	 * @param httpServletRequest the http servlet request
	 * @return the current group id
	 */
	public static long getCurrentGroupId(HttpServletRequest httpServletRequest) {
		return PortalContext.getConfiguration().getCurrentGroupId(httpServletRequest);
	}

	/**
	 * Gets the CMS user role for the logged user (in the current context)
	 *
	 * @param httpServletRequest the http servlet request
	 * @return the scopes with user roles for logged user
	 * @throws Exception the exception
	 */
	public static List<CMSUserRole> getCMSRoleForUserInTheScope(HttpServletRequest httpServletRequest) throws Exception {
		LOG.info("called getScopesWithThreddsRolesForLoggedUser");

		// DEV MODE
		if (!isWithinPortal()) {
			//CMSUserRole userRole = CMSUserRole.CATALOGUE_MODERATOR;
			CMSUserRole userRole = null;
			LOG.warn("\n\n\nDevelopment MODE is enabled, returning CMS user role: "+userRole);
			return Arrays.asList(userRole);
		}

		GCubeUser user = null;
		String scope = null;
		Long groupId = null;
		try {

			user = getCurrentUser(httpServletRequest);
			scope = getCurrentScope(httpServletRequest);
			groupId = getCurrentGroupId(httpServletRequest);
			List<CMSUserRole> cmsRoles = getCMSRoleForUserInTheGroupId(user, groupId);
			LOG.info("in the context: " + scope + ", returning the role: " + cmsRoles + " for user "
					+ user.getUsername());
			return cmsRoles;
		} catch (Exception e) {
			String errorMsg = "An error occurred on checking user roles. Refresh the page and try again.";
			LOG.error("An error occurred on checking user roles for user: " + user, e);
			throw new Exception(errorMsg);
		}
	}

	/**
	 * Gets the CMS role for user in the scope.
	 *
	 * @param user    the user
	 * @param groupId the group id
	 * @return the CMS role for user in the scope
	 */
	private static List<CMSUserRole> getCMSRoleForUserInTheGroupId(GCubeUser user, Long groupId) {
		if (user == null || groupId == null) {
			LOG.warn("called getCMSRoleForUserInTheScope with invalid parameter user: " + user + ", in the groupId: "
					+ groupId, ", returning null");
			return null;
		}

		LOG.info("called getCMSRoleForUserInTheScope user: " + user.getUsername() + ", in the groupId: " + groupId);
		try {
			RoleManager roleManager = new LiferayRoleManager();
			List<GCubeRole> roles = roleManager.listRolesByUserAndGroup(user.getUserId(), groupId);
			List<CMSUserRole> toReturn = new ArrayList<CMSUserRole>();
			if (roles != null) {
				for (GCubeRole gCubeRole : roles) {
					if (gCubeRole.getRoleName().equalsIgnoreCase(CMSUserRole.CATALOGUE_MODERATOR.getRoleName())) {
						toReturn.add(CMSUserRole.CATALOGUE_MODERATOR);
					}
				}
			}
			LOG.info("For user: " + user.getUsername() + " in the groupId: " + groupId
					+ " read and returing the role/s: " + toReturn);
			return toReturn;
		} catch (UserRetrievalFault | GroupRetrievalFault e) {
			LOG.error("An error occurred during getVreRoleForUser: " + user, e);
			return null;
		}
	}

}
