package org.gcube.portlets.widgets.ckancontentmoderator.shared;

 public enum CMSUserRole {
	
	CATALOGUE_MODERATOR("Catalogue-Moderator");
		
	private String name;
	
	private CMSUserRole(String name) {
		this.name = name;
	}
	
	public String getRoleName() {
		return this.name;
	}

}
