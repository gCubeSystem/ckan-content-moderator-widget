package org.gcube.portlets.widgets.ckancontentmoderator.shared;

import java.io.Serializable;

import org.gcube.datacatalogue.utillibrary.shared.ItemStatus;

import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * The Class CatalogueDataset.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Jun 14, 2021
 */
public class CatalogueDataset implements Serializable, IsSerializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 566645830900736239L;
	private String author;
	private String authorEmail;
	private String id;
	private String licenseId;
	private String maintainer;
	private String maintainerEmail;
	private String name;
	private String notes;
	private String ownerOrg;
	private ItemStatus status;
	private String title;
	private String url;
	private String version;
	private String extra_ItemURL;
	private String extra_SystemType;
	private long created;
	private long lastUpdate;

	/**
	 * Instantiates a new ckan dataset base.
	 */
	public CatalogueDataset() {
	}

	public String getAuthor() {
		return author;
	}

	public String getAuthorEmail() {
		return authorEmail;
	}

	public String getId() {
		return id;
	}

	public String getLicenseId() {
		return licenseId;
	}

	public String getMaintainer() {
		return maintainer;
	}

	public String getMaintainerEmail() {
		return maintainerEmail;
	}

	public String getName() {
		return name;
	}

	public String getNotes() {
		return notes;
	}

	public String getOwnerOrg() {
		return ownerOrg;
	}

	public ItemStatus getStatus() {
		return status;
	}

	public String getTitle() {
		return title;
	}

	public String getUrl() {
		return url;
	}

	public String getVersion() {
		return version;
	}

	public String getD4scienceItemURL() {
		return extra_ItemURL;
	}

	public long getCreated() {
		return created;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public void setAuthorEmail(String authorEmail) {
		this.authorEmail = authorEmail;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setLicenseId(String licenseId) {
		this.licenseId = licenseId;
	}

	public void setMaintainer(String maintainer) {
		this.maintainer = maintainer;
	}

	public void setMaintainerEmail(String maintainerEmail) {
		this.maintainerEmail = maintainerEmail;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public void setOwnerOrg(String ownerOrg) {
		this.ownerOrg = ownerOrg;
	}

	public void setStatus(ItemStatus status) {
		this.status = status;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public void setCreated(long created) {
		this.created = created;
	}

	public void setLastUpdate(long time) {
		this.lastUpdate = time;

	}

	public long getLastUpdate() {
		return lastUpdate;
	}

	public String getExtra_ItemURL() {
		return extra_ItemURL;
	}

	public void setExtra_ItemURL(String extra_ItemURL) {
		this.extra_ItemURL = extra_ItemURL;
	}

	public String getExtra_SystemType() {
		return extra_SystemType;
	}

	public void setExtra_SystemType(String extra_SystemType) {
		this.extra_SystemType = extra_SystemType;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("CatalogueDataset [author=");
		builder.append(author);
		builder.append(", authorEmail=");
		builder.append(authorEmail);
		builder.append(", id=");
		builder.append(id);
		builder.append(", licenseId=");
		builder.append(licenseId);
		builder.append(", maintainer=");
		builder.append(maintainer);
		builder.append(", maintainerEmail=");
		builder.append(maintainerEmail);
		builder.append(", name=");
		builder.append(name);
		builder.append(", notes=");
		builder.append(notes);
		builder.append(", ownerOrg=");
		builder.append(ownerOrg);
		builder.append(", status=");
		builder.append(status);
		builder.append(", title=");
		builder.append(title);
		builder.append(", url=");
		builder.append(url);
		builder.append(", version=");
		builder.append(version);
		builder.append(", extra_ItemURL=");
		builder.append(extra_ItemURL);
		builder.append(", extra_SystemType=");
		builder.append(extra_SystemType);
		builder.append(", created=");
		builder.append(created);
		builder.append(", lastUpdate=");
		builder.append(lastUpdate);
		builder.append("]");
		return builder.toString();
	}

}