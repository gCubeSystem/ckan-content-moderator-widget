package org.gcube.portlets.widgets.ckancontentmoderator.shared;

/**
 * The Enum DISPLAY_FIELD.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Apr 22, 2022
 */
public enum DISPLAY_FIELD {
	NAME("Name", "name"), TITLE("Title", "title"), AUTHOR("Author", "author_mail"),
	CREATED("Created", "metadata_created"), LAST_UPDATE("Last Update", "metadata_modified"),
	TYPE("Type", "extras_systemtype");

	String label;
	String jsonField;

	/**
	 * Instantiates a new display field.
	 *
	 * @param label     the label
	 * @param jsonField the json field
	 */
	private DISPLAY_FIELD(String label, String jsonField) {
		this.label = label;
		this.jsonField = jsonField;
	}

	/**
	 * Gets the label.
	 *
	 * @return the label
	 */
	public String getLabel() {
		return label;
	}

	/**
	 * Gets the json field.
	 *
	 * @return the json field
	 */
	public String getJsonField() {
		return jsonField;
	}

}