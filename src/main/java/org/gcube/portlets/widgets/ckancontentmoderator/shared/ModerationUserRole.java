package org.gcube.portlets.widgets.ckancontentmoderator.shared;

import java.io.Serializable;
import java.util.List;

/**
 * The Class ModerationUserRole.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Feb 21, 2022
 */
public class ModerationUserRole implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7002466635359684148L;
	private String username;
	private List<CMSUserRole> roles;

	/**
	 * Instantiates a new moderation user role.
	 */
	public ModerationUserRole() {
	}

	public ModerationUserRole(String username, List<CMSUserRole> roles) {
		super();
		this.username = username;
		this.roles = roles;
	}

	public String getUsername() {
		return username;
	}

	public List<CMSUserRole> getRoles() {
		return roles;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ModerationUserRole [username=");
		builder.append(username);
		builder.append(", roles=");
		builder.append(roles);
		builder.append("]");
		return builder.toString();
	}

}
