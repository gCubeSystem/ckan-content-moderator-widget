package org.gcube.portlets.widgets.ckancontentmoderator.shared;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class OperationReport implements Serializable {


	/**
	 * 
	 */
	private static final long serialVersionUID = 4960774282956107668L;
	private String operationType;
	private Map<String, String> errorMapItems = new HashMap<String, String>();
	private List<String> passedListItems = new ArrayList<String>();

	public OperationReport() {
	}

	public OperationReport(String operationType, List<String> passedListItems, Map<String, String> errorMapItems) {
		this.operationType = operationType;
		this.errorMapItems = errorMapItems;
		this.passedListItems = passedListItems;
	}

	public String getOperationType() {
		return operationType;
	}

	public Map<String, String> getErrorMapItems() {
		return errorMapItems;
	}

	public List<String> getPassedListItems() {
		return passedListItems;
	}

	public void setOperationType(String operationType) {
		this.operationType = operationType;
	}

	public void setErrorMapItems(Map<String, String> errorMapItems) {
		this.errorMapItems = errorMapItems;
	}

	public void setPassedListItems(List<String> passedListItems) {
		this.passedListItems = passedListItems;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("OperationReport [operationType=");
		builder.append(operationType);
		builder.append(", errorMapItems=");
		builder.append(errorMapItems);
		builder.append(", passedListItems=");
		builder.append(passedListItems);
		builder.append("]");
		return builder.toString();
	}

}
