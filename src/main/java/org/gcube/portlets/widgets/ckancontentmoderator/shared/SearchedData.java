/**
 *
 */
package org.gcube.portlets.widgets.ckancontentmoderator.shared;

import java.io.Serializable;
import java.util.List;


/**
 * The Class SearchedFolder.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Jun 16, 2021
 */
public class SearchedData implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6800997954077785719L;
	private List<CatalogueDataset> data;
	private int clientStartIndex = 0;
	private int limit;
	private int serverEndIndex = 0;
	private boolean isServerSearchFinished = false;
	private long totalItems;

	/**
	 * Instantiates a new searched folder.
	 */
	public SearchedData() {
	}

	/**
	 * Instantiates a new searched data.
	 *
	 * @param clientStartIndex       the client start index
	 * @param limit                  the limit
	 * @param serverEndIndex         the server end index
	 * @param isServerSearchFinished the is server search finished
	 */
	public SearchedData(int clientStartIndex, int limit, int serverEndIndex, boolean isServerSearchFinished) {

		this.clientStartIndex = clientStartIndex;
		this.limit = limit;
		this.serverEndIndex = serverEndIndex;
		this.isServerSearchFinished = isServerSearchFinished;
	}

	/**
	 * Gets the data.
	 *
	 * @return the data
	 */
	public List<CatalogueDataset> getData() {
		return data;
	}

	/**
	 * Gets the client start index.
	 *
	 * @return the client start index
	 */
	public int getClientStartIndex() {
		return clientStartIndex;
	}

	/**
	 * Gets the limit.
	 *
	 * @return the limit
	 */
	public int getLimit() {
		return limit;
	}

	/**
	 * Gets the server end index.
	 *
	 * @return the server end index
	 */
	public int getServerEndIndex() {
		return serverEndIndex;
	}

	/**
	 * Checks if is server search finished.
	 *
	 * @return true, if is server search finished
	 */
	public boolean isServerSearchFinished() {
		return isServerSearchFinished;
	}

	/**
	 * Sets the data.
	 *
	 * @param data the new data
	 */
	public void setData(List<CatalogueDataset> data) {
		this.data = data;
	}

	/**
	 * Sets the client start index.
	 *
	 * @param clientStartIndex the new client start index
	 */
	public void setClientStartIndex(int clientStartIndex) {
		this.clientStartIndex = clientStartIndex;
	}

	/**
	 * Sets the limit.
	 *
	 * @param limit the new limit
	 */
	public void setLimit(int limit) {
		this.limit = limit;
	}

	/**
	 * Sets the server end index.
	 *
	 * @param serverEndIndex the new server end index
	 */
	public void setServerEndIndex(int serverEndIndex) {
		this.serverEndIndex = serverEndIndex;
	}

	/**
	 * Sets the server search finished.
	 *
	 * @param isServerSearchFinished the new server search finished
	 */
	public void setServerSearchFinished(boolean isServerSearchFinished) {
		this.isServerSearchFinished = isServerSearchFinished;
	}

	/**
	 * Gets the total items.
	 *
	 * @return the total items
	 */
	public long getTotalItems() {
		return totalItems;
	}

	/**
	 * Sets the total items.
	 *
	 * @param totalItems the new total items
	 */
	public void setTotalItems(long totalItems) {
		this.totalItems = totalItems;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("SearchedData [data=");
		builder.append(data);
		builder.append(", clientStartIndex=");
		builder.append(clientStartIndex);
		builder.append(", limit=");
		builder.append(limit);
		builder.append(", serverEndIndex=");
		builder.append(serverEndIndex);
		builder.append(", isServerSearchFinished=");
		builder.append(isServerSearchFinished);
		builder.append(", totalItems=");
		builder.append(totalItems);
		builder.append("]");
		return builder.toString();
	}



}
