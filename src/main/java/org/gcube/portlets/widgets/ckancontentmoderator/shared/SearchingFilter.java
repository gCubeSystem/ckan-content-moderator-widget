package org.gcube.portlets.widgets.ckancontentmoderator.shared;

import java.io.Serializable;
import java.util.List;

/**
 * The Class SearchingFilter.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Apr 22, 2022
 */
public class SearchingFilter implements Serializable {

	/**
	 * The Enum ORDER.
	 *
	 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
	 * 
	 *         Apr 22, 2022
	 */
	public static enum ORDER {
		ASC("asc"), DESC("desc");

		String label;

		/**
		 * Instantiates a new order.
		 *
		 * @param label the label
		 */
		ORDER(String label) {
			this.label = label;
		}

		/**
		 * Gets the label.
		 *
		 * @return the label
		 */
		public String getLabel() {
			return label;
		}
	}

	/**
	 * The Enum LOGICAL_OP.
	 *
	 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
	 * 
	 *         Dec 15, 2021
	 */
	public static enum LOGICAL_OP {
		AND("AND"), OR("OR");

		String operator;

		/**
		 * Instantiates a new order.
		 *
		 * @param operator the operator
		 */
		LOGICAL_OP(String operator) {
			this.operator = operator;
		}

		/**
		 * Gets the operator.
		 *
		 * @return the operator
		 */
		public String getOperator() {
			return operator;
		}
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -4004094263090373626L;

	private List<ItemFieldDV> orderByFields;

	private ORDER order = ORDER.ASC;

	private List<WhereClause> conditions;

	/**
	 * Instantiates a new sort filter.
	 */
	public SearchingFilter() {

	}

	/**
	 * Instantiates a new sort filter.
	 *
	 * @param orderByFields the order by fields
	 * @param order         the order
	 */
	public SearchingFilter(List<ItemFieldDV> orderByFields, ORDER order) {
		this.orderByFields = orderByFields;
		this.order = order;
	}

	/**
	 * Instantiates a new sort filter.
	 *
	 * @param orderByFields the order by fields
	 * @param order         the order
	 * @param conditions    the conditions
	 */
	public SearchingFilter(List<ItemFieldDV> orderByFields, ORDER order, List<WhereClause> conditions) {
		this.orderByFields = orderByFields;
		this.order = order;
		this.conditions = conditions;
	}

	/**
	 * Gets the conditions.
	 *
	 * @return the conditions
	 */
	public List<WhereClause> getConditions() {
		return conditions;
	}

	/**
	 * Sets the conditions.
	 *
	 * @param conditions the new conditions
	 */
	public void setConditions(List<WhereClause> conditions) {
		this.conditions = conditions;
	}

	/**
	 * Gets the order by fields.
	 *
	 * @return the order by fields
	 */
	public List<ItemFieldDV> getOrderByFields() {
		return orderByFields;
	}

	/**
	 * Gets the order.
	 *
	 * @return the order
	 */
	public ORDER getOrder() {
		return order;
	}

	/**
	 * Sets the order by fields.
	 *
	 * @param orderByFields the new order by fields
	 */
	public void setOrderByFields(List<ItemFieldDV> orderByFields) {
		this.orderByFields = orderByFields;
	}

	/**
	 * Sets the order.
	 *
	 * @param order the new order
	 */
	public void setOrder(ORDER order) {
		this.order = order;
	}

	/**
	 * To string.
	 *
	 * @return the string
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("SearchingFilter [orderByFields=");
		builder.append(orderByFields);
		builder.append(", order=");
		builder.append(order);
		builder.append(", conditions=");
		builder.append(conditions);
		builder.append("]");
		return builder.toString();
	}

}
