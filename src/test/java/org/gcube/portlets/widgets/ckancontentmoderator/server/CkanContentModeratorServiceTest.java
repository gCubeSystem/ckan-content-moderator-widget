package org.gcube.portlets.widgets.ckancontentmoderator.server;

import static org.junit.Assert.fail;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.gcube.common.authorization.library.provider.SecurityTokenProvider;
import org.gcube.common.scope.api.ScopeProvider;
import org.gcube.datacatalogue.utillibrary.gcat.GCatCaller;
import org.gcube.datacatalogue.utillibrary.server.cms.CatalogueContentModeratorSystem;
import org.gcube.datacatalogue.utillibrary.shared.ItemStatus;
import org.gcube.datacatalogue.utillibrary.shared.jackan.model.CkanDataset;
import org.gcube.portlets.widgets.ckancontentmoderator.client.ContentModeratorWidgetConstants;
import org.slf4j.LoggerFactory;

public class CkanContentModeratorServiceTest {

	private String scope = "/gcube/devsec/devVRE";
	// private String scope = "/pred4s/preprod/Dorne";
	private String testUser = "francesco.mangiacrapa";
	// private String authorizationToken = "";
	// private String scope = "/gcube/devsec";
	private String authorizationToken = ""; // devVRE

	private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(CkanContentModeratorServiceTest.class);

	// @Test
	public void test() {
		fail("Not yet implemented");
	}

	//@Test
	public void loadItemsForStatus() {
		ScopeProvider.instance.set(scope);
		SecurityTokenProvider.instance.set(authorizationToken);
		ItemStatus itemStatus = ItemStatus.PENDING;
		try {
			CatalogueContentModeratorSystem cms = CatalogueCMSFactory.getFactory().getCMSPerScope(scope);
			Map<String, String> filters = new HashMap<String, String>(1);
			filters.put(ContentModeratorWidgetConstants.CKAN_FIELD_NAME_AUTHOR_MAIL,
					"francesco.mangiacrapa@isti.cnr.it");
			List<CkanDataset> items = cms.getListItemsForStatus(itemStatus, 20, 0, true, filters,
					GCatCaller.DEFAULT_SORT_VALUE);
			int i = 0;
			System.out.println("Datasets with status " + itemStatus + " are: " + items.size());
			for (CkanDataset ckanDataset : items) {
				System.out.println(i++ + ")Read dataset: " + ckanDataset);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
